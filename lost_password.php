<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

if (isset($_SESSION["form_id"]))
{
    $old_form_id = $_SESSION["form_id"];
}
else
{
    $old_form_id = "";
}
$form_id = "wapbb".generate_form_id();
$_SESSION["form_id"] = $form_id;
$button_id = "wapbb".generate_button_id("lp");
if (isset($_POST[$button_id]))
{
    $error = array();
    if ($old_form_id != $_POST["form_id"])
    {
        $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
    }
    else
    {
        if ($config_captcha == 1)
        {
            if (isset($_POST["captcha_code"]) && !empty($_POST["captcha_code"]))
            {
                $post_captcha_code = strip_tags($_POST["captcha_code"]);
                $captcha_key = $_SESSION["captcha_key"];
                $captcha_code = $_SESSION["captcha_code"];
                $word_is = xoft_decode($captcha_code, $captcha_key);
                if ($post_captcha_code != $word_is)
                {
                    $error["captcha"] = "Your verfication code is incorrect";
                }
            }
            else
            {
                $error["captcha"] = "Enter your unique security code";
            }
		}
        
        if (empty($error))
        {
            $username = strip_tags($_POST["username"]);
            $email = strip_tags($_POST["email"]);
            if (!empty($username))
            {
                $username_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE username = '".$username."' LIMIT 1");
                if (mysql_num_rows($username_query) == 1)
                {
                    echo "Username exist";
                }
                else
                {
                    $error["username"] = "Username does not exist";
                }
            }
            else if (!empty($email))
            {
                $email_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE email = '".$email."' LIMIT 1");
                if (mysql_num_rows($email_query) == 1)
                {
                    echo "Email exist";
                }
                else
                {
                    $error["email"] = "Email address does not exist";
                }
            }
            else
            {
                $error["form"] = "You must enter your username or email address";
            }
        }
    }
}

echo "<form action=\"lost_password.php\" method=\"post\">\n";
echo "<div class=\"content\">\n";

if (!empty($error["form"]))
{
	echo "<div class=\"message error\">".$error["form"]."</div>\n";
}
else if (!empty($error["email"]))
{
	echo "<div class=\"message error\">".$error["email"]."</div>\n";
}

echo "<label for=\"username\">Username:</label><br />\n";

if (!empty($error["username"]))
{
    echo "<input class=\"error\" type=\"text\" id=\"username\" name=\"username\" value=\"\" maxlength=\"20\" /><br />\n";
    echo "<span class=\"desc error\">".$error["username"]."</span><br /><br />\n";
}
else
{
    echo "<input type=\"text\" id=\"username\" name=\"username\" value=\"\" maxlength=\"20\" /><br /><br />\n";
}

echo "<b>OR</b><br /><br />\n";

echo "<label for=\"username\">Email Address:</label><br />\n";
echo "<input type=\"text\" id=\"email\" name=\"email\" value=\"\" maxlength=\"150\" /><br />\n";

if ($config_captcha == 1)
{
    $captcha_text = strtolower(generate_captcha());
    $captcha_key = strtolower(md5(uniqid(rand(), true)));
    $_SESSION["captcha_key"] = $captcha_key;
    $captcha_code = xoft_encode($captcha_text, $captcha_key);
    $_SESSION["captcha_code"] = $captcha_code;
    echo "<br />\n";
    echo "<label for=\"captcha_code\">Human Verification:</label><br />\n";
    echo "<label for=\"captcha_code\">".image(SITE_URL."/captcha.php?code=".$captcha_code, "Captcha")."</label><br />\n";
    if (!empty($error["captcha"]))
    {
        echo "<input class=\"error\" id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br />\n";
        echo "<span class=\"desc error\">".$error["captcha"]."</span><br />";
    }
    else
    {
        echo "<input id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br />";
    }
}

echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";

echo "</div>\n";

echo "<div class=\"buttons\">\n";
echo "<input class=\"button ibutton\" type=\"submit\" value=\"Request Password\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
echo "</div>\n";
echo "</form>\n";


include_once "themes/".$config_theme."/foot.php";
?>