<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";
if (is_logged())
{
    $error = array();
    if (isset($_SESSION["form_id"]))
    {
        $old_form_id = $_SESSION["form_id"];
    }
    else
    {
        $old_form_id = "";
    }
    $form_id = "wapbb".generate_form_id();
    $_SESSION["form_id"] = $form_id;
    $button_id = "wapbb".generate_button_id("new_topic");
    if (isset($_POST[$button_id]))
    {
        if ($forum == false)
        {
            $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
        }
        
        /*
        $form_location = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $new_referrer = $_SERVER["HTTP_REFERER"];
        if ($new_referrer !== $form_location)
        {
            $error["form"] = "<b>Referrer Missing or Mismatch:</b><br />It looks like you are trying to post remotely or you have blocked referrers on your user agent or browser.";
        }
		//*/
        
        if ($old_form_id != $_POST["form_id"])
        {
            $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
        }
        
        if ($user["last_post_time"] > ($time - 60))
        {
            $error["post_flood"] = "Flood control is enabled. Please wait at least 30 seconds before attempting to create new topic";
        }
        
        $title = strip_tags($_POST["title"]);
        if (empty($title))
        {
            $error["title"] = "You must enter a topic title";
        }
        elseif (strlen($title) < 2)
        {
            $error["title"] = "Topic title must be longer than 2 characters";
        }
        
        $description = strip_tags($_POST["description"]);
        
        //$post = strip_tags($_POST["post"], "<p><a><b><i><img><u><font>[url][img][URL][IMG][FONT][font]<sub><sup><span><li><size>[list][o][size][s][mail]");
        $post = $_POST["post"];
        if (empty($post))
        {
            $error["post"] = "You must enter a post";
        }
        
        if (isset($_POST["use_emo"]))
        {
            $use_emo = 1;
        }
        else
        {
            $use_emo = 0;
        }
        
        if (isset($_POST["use_sig"]))
        {
            $use_sig = 1;
        }
        else
        {
            $use_sig = 0;
        }
        
        /*
        $inpt_expl = "/(content-type|to:|bcc:|cc:|document.cookie|document.write|onclick|onload)/i";
        if (preg_match($inpt_expl, $title) || preg_match($inpt_expl, $description) || preg_match($inpt_expl, $post))
        {
            $error["post"] = "<b>Injection Exploit Detected:</b><br />It seems that you are possibly trying to apply a header or input injection exploit in form. If you are, please stop at once! If not, check to make sure you haven&#8217;t entered <strong>content-type</strong>, <strong>to:</strong>, <strong>bcc:</strong>, <strong>cc:</strong>, <strong>document.cookie</strong>, <strong>document.write</strong>, <strong>onclick</strong>, or <strong>onload</strong> in any of the form inputs. If you have and you&#8217;re trying to send a legitimate message, for security reasons, please find another way of communicating these terms.";
        }
		//*/

		if ($config_attachment == 1 && empty($error))
		{
			if ((!empty($_FILES["attach"])) && ($_FILES["attach"]["error"] == 0))
			{
				$file_name = basename($_FILES["attach"]["name"]);
				$file_ext = substr($file_name, strrpos($file_name, ".") + 1);
				$file_type_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments_type WHERE atype_extension = '".$file_ext."'");
				if (mysql_numrows($file_type_query) == 1)
				{
					$file_size = $_FILES["attach"]["size"];
					if ($file_size < $config_attach_max_size)
					{
						$new_name = "attachments/".uniqid(mt_rand(), true);
						if (!file_exists($new_name))
						{
							if ((move_uploaded_file($_FILES["attach"]["tmp_name"], $new_name)))
                            {
								$attach_name = $file_name;
								$attach_ext = $file_ext;
								$attach_type = $_FILES["attach"]["type"];
								$attach_size = $file_size;
								$attach_upload_time = $time;
							}
							else
							{
								$error["attach"] = "A problem occurred during file upload!";
							}
						}
						else
						{
							$error["attach"] = htmlspecialchars($file_name)." already exists";
						}
					}
					else
					{
						$error["attach"] = "Your file size is too big. Max allowed is ".file_size($config_attach_max_size);
					}
				}
				else
				{
					$allowed_ext = "";
					$ext_query = mysql_query("SELECT atype_extension FROM ".SQL_TABLE_PERFIX."attachments_type");
					while ($ext = mysql_fetch_array($ext_query))
					{
						$allowed_ext .= ".".$ext["atype_extension"]." ";
					}
					$error["attach"] = "Only ".$allowed_ext." extension files are accepted for upload";
				}
			}
		}
        
        if (empty($error))
        {
            $topic_query = mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."topics(title, description, topic_forum_id, start_time, starter_id, starter_name) VALUES('".$title."', '".$description."', '".$forum_id."', '".$time."', '".$user["user_id"]."', '".$user["username"]."')");
            $topic_id = mysql_insert_id();
            if ($topic_query == false)
            {
                $error["form"] = "An error occured while inserting your topic data. Please try again later.";
            }
            else
            {
                $post_query = mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."posts(post_content, post_topic_id, post_time, poster_id, poster_name, use_emo, use_sig) VALUES('".$post."', '".$topic_id."', '".$time."', '".$user["user_id"]."', '".$user["username"]."', '".$use_emo."', '".$use_sig."')");
                $post_id = mysql_insert_id();
                if ($post_query == false)
                {
                    mysql_query("DELETE FROM wapbb_topics WHERE topic_id = '".$topic_id."'");
                    $error["form"] = "An error occured while inserting your post data. Please try again later.";
                }
                else
                {
                    if ($config_attachment == 1)
                    {
                        if ((!empty($_FILES["attach"])) && ($_FILES["attach"]["error"] == 0))
                        {
                            mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."attachments(attach_file, attach_file_ext, attach_location, attach_filesize, attach_time, attach_post_id, attach_user_id, attach_user_name) VALUES('".$attach_name."', '".$attach_ext."', '".$new_name."', '".$attach_size."', '".$time."', '".$post_id."', '".$user["user_id"]."', '".$user["username"]."')");
                        }
                    }
                    mysql_query("UPDATE ".SQL_TABLE_PERFIX."topics SET last_post_id = '".$post_id."', last_post_time = '".$time."', last_poster_id = '".$user["user_id"]."', last_poster_name= '".$user["username"]."' WHERE topic_id = '".$topic_id."'");
                    mysql_query("UPDATE ".SQL_TABLE_PERFIX."forums SET num_topics = num_topics + 1, num_posts = num_posts + 1, last_post_id = '".$post_id."', last_post_time = '".$time."', last_poster_id = '".$user["user_id"]."', last_poster_name = '".$user["username"]."' WHERE forum_id = '".$forum_id."'");
                    mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET num_posts = num_posts + 1, num_topics = num_topics + 1, last_post_time = '".$time."', WHERE user_id = '".$user["user_id"]."'");
                        
                    mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."topic_user_map (user_id, topic_id, topic_forum_id, has_read, read_time) VALUES ('".$user["user_id"]."', '".$topic_id."', '".$forum_id."', '1', '".$time."')");
                    mysql_query("COMMIT;");
                    echo "<div class=\"content\">\n";
                    echo "<div class=\"message\">\n";
                    echo "<b>New topic created successfully!</b><br />\n";
                    echo anchor("topic.php?tid=".$topic_id, "Click here to view", "View Topic")."\n";
                    echo "</div>\n";
                    echo "</div>\n";
                    include_once "themes/".$config_theme."/foot.php";
                }
            }
		}
	}
    elseif (isset($_POST["preview_post"]))
    {
        $title = strip_tags($_POST["title"]);
        $description = strip_tags($_POST["description"]);
        $post = $_POST["post"];
        $post = reverse_mysql_real_escape_string($post);
        echo "<div class=\"title\">Post Preview: <i>".$title."</i></div>\n";
        echo "<div class=\"posts\">\n";
        if (empty($post))
        {
            echo "<div class=\"message error\">You must enter a post</div>\n";
        }
        else
        {
            echo "<div class=\"post\">\n";
            echo "<div class=\"post_author\">\n";
            echo image("images/default_thumb.png", null, null, null, ".photo off")."\n";
            echo anchor("user.php?uid=".$user["user_id"], htmlspecialchars($user["username"]), "View ".$user["username"]."'s Profile", ".b")."<br />\n";
            echo "<span class=\"desc smaller\">".date("j F Y - h:i A", $time)."</span>";
            echo "</div>\n";
            echo "<div class=\"post_content\">\n";
            if (isset($_POST["use_emo"]))
            {
                echo emoticons(bbcode($post))."<br />\n";
            }
            else
            {
                echo bbcode($post)."<br />\n";
            }
            echo "</div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
    }
    elseif ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST[$button_id]))
	{
	   $error["form"] = "<b>Submit Variable Mismatch:</b><br />\nIt looks like you are trying to post remotely as the submit variable is unmatched.";
	}
	else
	{
		$title = "";
		$description = "";
		$post = "";
	}

	echo "<div class=\"title\">Posting A New Topic In A ".htmlspecialchars($forum["forum_name"])."</div>\n";
	echo "<form method=\"POST\" action=\"new_topic.php?fid=".$forum_id."\" enctype=\"multipart/form-data\">\n";
	echo "<div class=\"content\">\n";
	if (!empty($error))
	{
		if (!empty($error["form"]))
		{
			echo "<div class=\"message error\">\n";
			echo $error["form"]."<br />\n";
			echo "</div>\n";
		}
        elseif (!empty($error["post_flood"]))
		{
			echo "<div class=\"message notice\">\n";
			echo $error["post_flood"]."<br />\n";
			echo "</div>\n";
		}
        elseif (!empty($error["title"]))
		{
			echo "<div class=\"message notice\">\n";
			echo "<label for=\"title\">".$error["title"]."</label><br />\n";
			echo "</div>\n";
		}
        elseif (!empty($error["post"]))
		{
			echo "<div class=\"message notice\">\n";
			echo "<label for=\"post\">".$error["post"]."</label><br />\n";
			echo "</div>\n";
		}
        elseif (!empty($error["attach"]))
		{
			echo "<div class=\"message error\">\n";
			echo "<label for=\"post\">".$error["attach"]."</label><br />\n";
			echo "</div>\n";
		}
	}
	echo "<label for=\"title\">Topic Title:</label><br />\n";
	echo "<input type=\"text\" id=\"title\" name=\"title\" value=\"".$title."\" maxlength=\"50\" /><br /><br />\n";
	echo "<label for=\"description\">Topic Description <span class=\"desc\">(Optional)</span>:</label><br />\n";
	echo "<input type=\"text\" id=\"description\" name=\"description\" value=\"".$description."\" maxlength=\"100\" /><br /><br />\n";
	echo "<label for=\"post\">Topic:</label><br />\n";
	echo "<textarea id=\"post\" name=\"post\" rows=\"10\" cols=\"40\">".$post."</textarea><br /><br />\n";

	if ($config_attachment == 1)
	{
		echo "Attachment <span class=\"desc\">(Optional)</span>:<br />\n";
		echo "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".$config_attach_max_size."\" />";

		/*
		$accept = "";
		$attachments_type_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments_type");
		while ($attachments_type = mysql_fetch_array($attachments_type_query))
		{
		$accept .= ",".$attachments_type["atype_mimetype"];
		}
		$accept = substr($accept, strpos($accept, ",") + 1);
		*/
		echo "<input type=\"file\" name=\"attach\" /><br /><br />\n";
	}

	echo "<input type=\"checkbox\" checked=\"checked\" id=\"use_emo\" name=\"use_emo\" value=\"1\" /> <label for=\"use_emo\">Enable emoticons?</label><br />\n";
    //echo "<input type=\"checkbox\" checked=\"checked\" id=\"use_sig\" name=\"use_sig\" value=\"1\" /> <label for=\"use_sig\">Enable signature?</label><br />\n";
	echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
	echo "</div>\n";
	echo "<div class=\"buttons\">\n";
	echo "<input class=\"button ibutton\" type=\"submit\" value=\"Post New Topic\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
    echo "<input class=\"button ibutton\" type=\"submit\" value=\"Preview Post\" name=\"preview_post\" id=\"".$button_id."\" />\n";
	//echo " or ".anchor("forum.php?fid=".$forum_id, "Cancel", "Cancel")."\n";
	echo "</div>\n";
	echo "</form>\n";
}
else
{
	login_form("You must be log in to create new topic.", "notice");
}
include_once "themes/".$config_theme."/foot.php";
?>