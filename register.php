<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";

if (is_logged())
{
	header("Location: ".SITE_URL);
	exit;
}

include_once "themes/".$config_theme."/index.php";

if (isset($_POST["agree_to_terms"]))
{
    $agree_to_terms = 1;
}
else
{
    $agree_to_terms = 0;
}
if ($agree_to_terms == 1)
{
    $button_id = "wapbb".generate_button_id("register");
	if (isset($_POST[$button_id]))
	{
		$error = array();
        
        /*
        $form_location = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        
        echo $form_location."<br />\n";
        echo $http_referer."<br />\n";
        
        $form_location = strtolower(str_replace(array("http", "www", "&", "/", "#", "\\", ":", "%", "|", "^", ";", "@", "?", "+", "$", ".", "~", "-", "=", "_", " ", ), '', $form_location));
        $new_referrer = strtolower(str_replace(array("http", "www", "&", "/", "#", "\\", ":", "%", "|", "^", ";", "@", "?", "+", "$", ".", "~", "-", "=", "_", " ", ), "", $http_referer));
        if ($new_referrer !== $form_location)
        {
            $error["form"] = "<b>Referrer Missing or Mismatch:</b><br />It looks like you are trying to post remotely or you have blocked referrers on your user agent or browser.";
        }
        //*/
        
        if ($_SESSION["form_id"] != $_POST["form_id"])
        {
            $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
        }
        
		$username = $_POST["username"];
        $username = strip_tags($username);
        if ($username != "" && strlen($username) >= 3 && strlen($username) <= 20)
        {
            if (preg_match("/^[A-Za-z0-9\._-]{3,20}+$/", $username))
            {
                $check_user_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE username = '".$username."' LIMIT 1");
                if (mysql_num_rows($check_user_query) >= 1)
                {
                    $error["username"] = "The username is already taken by another member";
                }
            }
            else
            {
                $invalid_chars = "";
                for ($i = 0; $i < strlen($username); $i++)
                {
                    if (!preg_match("/^[A-Za-z0-9\._-]+$/", $username[$i]))
                    {
                        if ($username[$i] == " ")
                        {
                            $invalid_chars .= "(space)";
                        }
                        else
                        {
                            $invalid_chars .= htmlspecialchars($username[$i])." ";
                        }
                    }
                }
                $error["username"] = "Contains illegal character: ".$invalid_chars;
            }
        }
        else
        {
            $error["username"] = "The username must be between 3 and 20 characters";
        }
        
        $email = $_POST["email"];
        $email = strip_tags($email);
        $email2 = $_POST["email2"];
        $email2 = strip_tags($email2);
		if ($email != "")
		{
			if ($email == $email2)
            {
                if (check_email($email) == true)
                {
                    $email_registered_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE email = '".$email."' LIMIT 1");
                    if (mysql_num_rows($email_registered_query) >= 1)
                    {
                        $error["email"] = "Already someone registered with email address you entered";
                    }
                }
                else
                {
                    $error["email"] = "The email address you entered is invalid (ex: name@domain.com)";
                }
            }
            else
            {
                $error["email"] = "The email address you entered as confirmation does not match the original one. Please confirm your email address correctly.";
            }
		}
        else
        {
            $error["email"] = "You did not enter an email address. Please enter one.";
        }
        
        $password = $_POST["password"];
        $password = strip_tags($password);
        $password2 = $_POST["password2"];
        $password2 = strip_tags($password2);
        if ($password != $password2)
		{
			$error["password"] = "The password fields did not match";
		}
		elseif (strlen($password) > 32 || strlen($password) < 3)
		{
			$error["password"] = "Password must be at least 3 characters in length and at most 32";
		}
        
        if ($config_captcha == 1)
        {
            if (isset($_POST["captcha_code"]) && !empty($_POST["captcha_code"]))
            {
                $post_captcha_code = strip_tags($_POST["captcha_code"]);
                $captcha_key = $_SESSION["captcha_key"];
                $captcha_code = $_SESSION["captcha_code"];
                $word_is = xoft_decode($captcha_code, $captcha_key);
                if ($post_captcha_code != $word_is)
                {
                    $error["captcha"] = "Your verfication code is incorrect";
                }
            }
            else
            {
                $error["captcha"] = "Enter your unique security code";
            }
		}

		if (empty($error))
		{
			$password_salt = passsword_salt();
			$password_hash = password_hash($password_salt, $password);
			echo "<div class=\"content\">\n";
			if ($config_accoutn_activation == 0)
			{
				mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."users (username, password_hash, password_salt, email, ip_address, joined) VALUES ('".$username."', '".$password_hash."', '".$password_salt."', '".$email."', '".$ip."', '".$time."')");
				echo "Thank you ".$username." Your registration has been submitted. Go here to ".anchor("login.php", "Login", "Login to your account")."\n";
			}
			else
			{
				$day = date("U");
				$seedval = $day % 100000;
				srand($seedval);
				$verification_key = rand(1000000, 2000000);
                                
				mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."users (username, password_hash, password_salt, email, ip_address, joined, verification_key) VALUES ('".$username."', '".$password_hash."', '".$password_salt."', '".$email."', '".$ip."', '".$time."', '".$verification_key."')");
                
				//mail($email, "Account Activation at ".SITE_TITLE, $username.",\n\nTo complete the registration process on ".SITE_TITLE.", you will need to go to the URL below in your web browser.\n\nmember.php?action=activate&amp;user=".$username."&amp;key=".$verification_key."\n\nIf the above link does not work correctly, go to\n\nmember.php?action=activate\n\nYou will need to enter the following:\n\nUsername: ".$username."\nActivation Code: ".$verification_key."\n\nThank you,\n".SITE_TITLE." Staff") or die("Can't send mail");

				echo "Thank you ".$username.". Your registration has been submitted. Go here to ".anchor("login.php", "Login", "Login to your account")."<br /><br />\n";
                
                echo "The board administrator has chosen to require validation for all email addresses. Within the next 10 minutes (usually instantly) you'll receive an email with instructions on the next step. Don't worry, it won't take long before you can post!<br /><br />\n";
				echo "The email has been sent to ".$email."\n";
			}
			if ($config_admin_notify == 1)
			{
				//mail($config_admin_email, "Registration at ".SITE_TITLE, "This is just to inform you that ".$username." has just registered in your forums.\nYou can view ".$username."'s profile here:\nmember.php?user=".$username);
			}
			echo "</div>\n";
			include_once "themes/".$config_theme."/foot.php";
		}
	}
	else
	{
		$username = "";
		$email = "";
		$email2 = "";
	}
    
	echo "<form method=\"post\" action=\"register.php\">\n";
	echo "<div class=\"content\">\n";
    
    if (!empty($error["form"]))
    {
		echo "<div class=\"message error\">".$error["form"]."</div>\n";
    }
    
	echo "<label for=\"username\">Choose a Username:</label><br />\n";
	if (!empty($error["username"]))
	{
		echo "<input class=\"error\" id=\"username\" type=\"text\" name=\"username\" value=\"".$_POST["username"]."\" maxlength=\"20\" /><br />\n";
		echo "<span class=\"desc error\">".$error["username"]."</span><br />\n";
	}
	else
	{
		echo "<input id=\"username\" type=\"text\" name=\"username\" value=\"".$username."\" maxlength=\"20\" /><br />\n";
	}
	echo "<span class=\"desc\">The name you'll log in with.</span><br /><br />\n";

	echo "<label for=\"email\">Enter your Email address:</label><br />\n";
	if (!empty($error["email"]))
	{
		echo "<input class=\"error\" id=\"email\" type=\"text\" name=\"email\" value=\"".$email."\" maxlength=\"150\" /><br />\n";
		echo "<span class=\"desc error\">".$error["email"]."</span><br />\n";
	}
	else
	{
		echo "<input id=\"email\" type=\"text\" name=\"email\" value=\"".$email."\" maxlength=\"150\" /><br />\n";
	}
	echo "<span class=\"desc\">So that we can verify your identity, and keep you updated</span><br /><br />\n";

	echo "<label for=\"email2\">Re-enter your Email address:</label><br />\n";
	echo "<input id=\"email2\" type=\"text\" name=\"email2\" value=\"".$email2."\" maxlength=\"150\" /><br /><br />\n";

	echo "<label for=\"password\">Choose your Password:</label><br />\n";
	if (!empty($error["password"]))
	{
		echo "<input class=\"error\" id=\"password\" type=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br />\n";
		echo "<span class=\"desc error\">".$error["password"]."</span><br />\n";
	}
	else
	{
		echo "<input id=\"password\" type=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br />\n";
	}
	echo "<span class=\"desc\">You should choose a strong password, between 3 and 32 characters</span><br /><br />\n";

	echo "<label for=\"password2\">Re-enter your Password:</label><br />\n";
	echo "<input id=\"password2\" type=\"password\" name=\"password2\" value=\"\" maxlength=\"32\" /><br />";

	if ($config_captcha == 1)
	{
        $captcha_text = strtolower(generate_captcha());
        $captcha_key = strtolower(md5(uniqid(rand(), true)));
        $_SESSION["captcha_key"] = $captcha_key;
		$captcha_code = xoft_encode($captcha_text, $captcha_key);
        $_SESSION["captcha_code"] = $captcha_code;
        echo "<br />\n";
		echo "<label for=\"captcha_code\">Human Verification:</label><br />\n";
        echo "<span class=\"desc\">Please enter the text contained within the image on the right in to the text box below it. This process is used to prevent automated signups.</span><br />\n";
		echo "<label for=\"captcha_code\">".image(SITE_URL."/captcha.php?code=".$captcha_code, "Captcha")."</label><br />\n";
		if (!empty($error["captcha"]))
		{
			echo "<input class=\"error\" id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br />\n";
			echo "<span class=\"desc error\">".$error["captcha"]."</span><br />";
		}
		else
		{
			echo "<input id=\"captcha_code\" name=\"captcha_code\" type=\"text\" value=\"\" /><br />";
		}
	}
	echo "\n<input type=\"hidden\" name=\"agree_to_terms\" id=\"agree_to_terms\" value=\"1\" />\n";
    $form_id = "wapbb".generate_form_id();
    $_SESSION["form_id"] = $form_id;
    echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
	echo "</div>\n";
	echo "<div class=\"buttons\">\n";
	echo "<input class=\"button ibutton\" type=\"submit\" value=\"Submit Registration!\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
	echo "or ".anchor(SITE_URL, "Cancel", "Cancel registration")."\n";
	echo "</div>\n";
	echo "</form>\n";
}
else
{
	echo "<form method=\"post\" action=\"register.php\">\n";
	echo "<div class=\"content\">\n";
	if ($_SERVER["REQUEST_METHOD"] == "POST" && $agree_to_terms == 0)
	{
		echo "<div class=\"message error\">You must agree to the registration terms.</div>\n";
	}
	echo "<b>Forum Terms &amp; Rules</b><br /><br />\n";
	echo "Please take a moment to review these rules detailed below. If you agree with them and wish to proceed with the registration, simply click the \"Register\" button below. To cancel this registration, simply hit the 'back' button on your browser.<br /><br />\n";
	echo "Please remember that we are not responsible for any messages posted. We do not vouch for or warrant the accuracy, completeness or usefulness of any message, and are not responsible for the contents of any message.<br /><br />\n";
	echo "The messages express the views of the author of the message, not necessarily the views of this bulletin board. Any user who feels that a posted message is objectionable is encouraged to contact us immediately by email. We have the ability to remove objectionable messages and we will make every effort to do so, within a reasonable time frame, if we determine that removal is necessary.
<br /><br />\n";
	echo "You agree, through your use of this service, that you will not use this bulletin board to post any material which is knowingly false and/or defamatory, inaccurate, abusive, vulgar, hateful, harassing, obscene, profane, sexually oriented, threatening, invasive of a person's privacy, or otherwise violative of any law.<br /><br />\n";
	echo "You agree not to post any copyrighted material unless the copyright is owned by you or by this bulletin board.<br /><br />\n";
	echo "<input type=\"checkbox\" id=\"agree_cbox\" name=\"agree_to_terms\" value=\"1\" /><label class=\"b\" for=\"agree_cbox\"> I have read, understood and agree to these rules and conditions</label><br />\n";
	echo "</div>\n";
	echo "<div class=\"buttons\">\n";
	echo "<input class=\"button ibutton\" type=\"submit\" value=\"Continue Registration\" />\n";
	echo "or ".anchor(SITE_URL, "Cancel", "Cancel registration")."\n";
	echo "</div>\n";
	echo "</form>\n";
}

include_once "themes/".$config_theme."/foot.php";
?>