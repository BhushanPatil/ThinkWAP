<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

if (is_logged())
{
    if (isset($_GET["page"]))
    {
        $page = abs(intval($_GET["page"]));
    }
    else
    {
        $page = 1;
    }
    if ($action == "compose")
    {
        if (isset($_GET["to"]) && !empty($_GET["to"]))
        {
            $post_to = $_GET["to"];
        }
        elseif (isset($_POST["to"]) && !empty($_POST["to"]))
        {
            $post_to = $_POST["to"];
        }
        else
        {
            $post_to = "";
        }
        $post_to = strip_tags($post_to);
        
        if (isset($_GET["subject"]) && !empty($_GET["subject"]))
        {
            $post_subject = $_GET["subject"];
        }
        elseif (isset($_POST["subject"]) && !empty($_POST["subject"]))
        {
            $post_subject = $_POST["subject"];
        }
        else
        {
            $post_subject = "";
        }
        $post_subject = strip_tags($post_subject);
        
        if (isset($_GET["message"]) && !empty($_GET["message"]))
        {
            $post_message = $_GET["message"];
        }
        elseif (isset($_POST["message"]) && !empty($_POST["message"]))
        {
            $post_message = $_POST["message"];
        }
        else
        {
            $post_message = "";
        }
        
        $error = array();
        if (isset($_SESSION["form_id"]))
        {
            $old_form_id = $_SESSION["form_id"];
        }
        else
        {
            $old_form_id = "";
        }
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("compose");
        if (isset($_POST[$button_id]))
        {
            if ($old_form_id != $_POST["form_id"])
            {
                $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
            }
            
            if (empty($post_to))
            {
                $error["to"] = "You must enter at least one recipients username in the 'To' field";
            }
            else
            {
                $recipients = explode(",", $post_to);
                foreach($recipients as $recipient)
                {
                    $recipient = trim($recipient);
                    $recipient_user = get_user($recipient);
                    if ($recipient_user == false)
                    {
                        $error["to"] = "Recipient you specified is not registered username. Invalid recipient: ".$recipient;
                    }
                    elseif($recipient_user["user_id"] == $user["user_id"])
                    {
                        $error["to"] = "You can not send a message to yourself.";
                    }
                    else
                    {
                        $recipients_data[$recipient_user["user_id"]] = $recipient_user["username"];
                    }
                }
            }
            
            if (empty($post_subject))
            {
                $error["subject"] = "Please enter a subject";
            }
            elseif($post_subject > 85)
            {
                $error["subject"] = "The subject must be between 1 and 85 characters";
            }
            
            if (empty($post_message))
            {
                $error["message"] = "Please enter a message to be sent";
            }
            
            if (isset($_POST["use_emo"]))
            {
                $use_emo = 1;
            }
            else
            {
                $use_emo = 0;
            }
            
            if (isset($_POST["use_sig"]))
            {
                $use_sig = 1;
            }
            else
            {
                $use_sig = 0;
            }
            
            if (isset($_POST["read_receipt"]))
            {
                $read_receipt = 1;
            }
            else
            {
                $read_receipt = 0;
            }
            
            if (empty($error))
            {
                foreach($recipients_data as $recipient_id => $recipient_name)
                {
                    $compose_message_query = mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."private_messages(from_id, from_name, to_id, to_name, subject, message, send_time, read_receipt, use_emo, use_sig) VALUES('".$user["user_id"]."', '".$user["username"]."', '".$recipient_id."', '".$recipient_name."', '".$post_subject."', '".$post_message."', '".$time."', '".$read_receipt."', '".$use_emo."', '".$use_sig."')");
                    if ($compose_message_query == false)
                    {
                        $error["form"] = "An error occured while inserting your message data. Please contact the Administrator.";
                    }
                    else
                    {
                        echo "<div class=\"content\">\n";
                        echo "<div class=\"message\">\n";
                        echo "<b>Your private message has successfully been sent.</b><br />\n";
                        echo anchor("pm.php", "Return to your inbox", "View TopicReturn to your private message inbox")."\n";
                        echo "</div>\n";
                        echo "</div>\n";
                        include_once "themes/".$config_theme."/foot.php";
                    }
                }
            }
        }
        
        echo "<div class=\"content\">";
        echo anchor("pm.php", "Inbox", "Inbox", ".b")."<br />\n";
        echo anchor("pm.php?action=outbox", "Outbox", "Outbox", ".b")."<br />\n";
        echo "</div>\n";
        
        if (isset($_POST["preview_message"]))
        {
            $post_message = reverse_mysql_real_escape_string($post_message);
            
            echo "<div class=\"title\">Message Preview: <i>".$post_subject."</i></div>\n";
            echo "<div class=\"posts\">\n";
            if (empty($post_message))
            {
                echo "<div class=\"message notice\">\n";
                echo "<label for=\"message\">You must enter a Message</label><br />\n";
                echo "</div>\n";
            }
            else
            {
                echo "<div class=\"post\">\n";
                echo "<div class=\"post_author\">\n";
                echo image("images/default_thumb.png", null, null, null, ".photo off")."\n";
                echo anchor("user.php?uid=".$user["user_id"], htmlspecialchars($user["username"]), "View ".$user["username"]."'s Profile", ".b")."<br />\n";
                echo "<span class=\"desc smaller\">".date("j F Y - h:i A", $time)."</span>";
                echo "</div>\n";
                echo "<div class=\"post_content\">\n";
                if (isset($_POST["use_emo"]))
                {
                    echo emoticons(bbcode($post_message))."<br />\n";
                }
                else
                {
                    echo bbcode($post_message)."<br />\n";
                }
                echo "</div>\n";
                echo "</div>\n";
            }
            echo "</div>\n";
        }
        elseif ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST[$button_id]))
        {
            $error["form"] = "<b>Submit Variable Mismatch:</b><br />\nIt looks like you are trying to post remotely as the submit variable is unmatched.";
        }
        
        echo "<div class=\"title\">Compose New Message</div>\n";
        echo "<form method=\"post\" action=\"pm.php?action=compose\">\n";
        echo "<div class=\"content\">\n";
        if (!empty($error))
        {
            if (!empty($error["form"]))
            {
                echo "<div class=\"message error\">\n";
                echo $error["form"]."<br />\n";
                echo "</div>\n";
            }
            elseif (!empty($error["to"]))
            {
                echo "<div class=\"message notice\">\n";
                echo "<label for=\"to\">".$error["to"]."</label><br />\n";
                echo "</div>\n";
            }
            elseif (!empty($error["subject"]))
            {
                echo "<div class=\"message notice\">\n";
                echo "<label for=\"subject\">".$error["subject"]."</label><br />\n";
                echo "</div>\n";
            }
            elseif (!empty($error["message"]))
            {
                echo "<div class=\"message notice\">\n";
                echo "<label for=\"message\">".$error["message"]."</label><br />\n";
                echo "</div>\n";
            }
        }
        echo "<label for=\"to\">To:</label><br />\n";
        echo "<input type=\"text\" id=\"to\" name=\"to\" value=\"".$post_to."\" /><br /><br />\n";
        
        echo "<label for=\"subject\">Subject:</label><br />\n";
        echo "<input type=\"text\" id=\"subject\" name=\"subject\" value=\"".$post_subject."\" maxlength=\"50\" /><br /><br />\n";
        
        echo "<label for=\"message\">Message:</label><br />\n";
        echo "<textarea id=\"message\" name=\"message\" rows=\"10\" cols=\"40\" >".$post_message."</textarea><br /><br />\n";
        
        echo "<input type=\"checkbox\" checked=\"checked\" id=\"use_emo\" name=\"use_emo\" value=\"1\" /> <label for=\"use_emo\">Enable emoticons?</label><br />\n";
        
        //echo "<input type=\"checkbox\" id=\"read_receipt\" name=\"read_receipt\" value=\"1\" /> <label for=\"use_emo\">Request read receipt</label><br />\n";
        
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        echo "</div>\n";
        
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Send Message\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Preview Post\" name=\"preview_message\" id=\"preview_message\" />\n";
        echo "</div>\n";
        echo "</form>\n";
    }
    elseif ($action == "reply")
    {
        if (isset($_GET["message"]) && !empty($_GET["message"]))
        {
            $post_message = $_GET["message"];
        }
        elseif (isset($_POST["message"]) && !empty($_POST["message"]))
        {
            $post_message = $_POST["message"];
        }
        else
        {
            $post_message = "";
        }
        
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("compose");
        if (isset($_POST[$button_id]))
        {
            
        }
        if ($message)
        {
            echo "<div class=\"title\">Compose Reply</div>\n";
            echo "<form method=\"post\" action=\"pm.php?action=compose\">\n";
            echo "<div class=\"content\">\n";
            echo "<label for=\"message\">Message:</label><br />\n";
            echo "<textarea id=\"message\" name=\"message\" rows=\"10\" cols=\"40\" >".$post_message."</textarea><br /><br />\n";
            echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
            echo "</div>\n";
            
            echo "<div class=\"buttons\">\n";
            echo "<input class=\"button ibutton\" type=\"submit\" value=\"Send Message\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
            echo "<input class=\"button ibutton\" type=\"submit\" value=\"Preview Post\" name=\"preview_message\" id=\"preview_message\" />\n";
            echo "</div>\n";
            
            echo "</form>\n";
        }
    }
    elseif ($action == "read")
    {
        echo "<div class=\"content\">";
        echo anchor("pm.php?action=compose", "Compose", "Compose", ".b")."<br />\n";
        echo anchor("pm.php", "Inbox", "Inbox", ".b")."<br />\n";
        echo anchor("pm.php?action=outbox", "Outbox", "Outbox", ".b")."<br />\n";
        echo "</div>\n";
        
        if ($message)
        {
            echo "<div class=\"title\">".htmlspecialchars($message["subject"])."</div>\n";
            echo "<div class=\"posts\">\n";
            $messages_count_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."private_messages WHERE pm_id = '".$pm_id."' OR reply_to = '".$pm_id."'");
            $messages_count = mysql_fetch_array($messages_count_query);
            $messages_count = $messages_count[0];
            $pages = ceil($messages_count / $config_message_replies_per_page);
            $count_start = ($page - 1) * $config_message_replies_per_page;
            $message_start = $count_start + 1;
            $message_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."private_messages WHERE pm_id = '".$pm_id."' OR reply_to = '".$pm_id."' ORDER BY send_time LIMIT ".$count_start.", ".$config_message_replies_per_page."");
            while ($message = mysql_fetch_array($message_query))
            {
                $message["message"] = bbcode($message["message"]);
                if ($message["use_emo"] == 1)
                {
                    $message["message"] = emoticons($message["message"]);
                }
                echo "<div class=\"post\">\n";
                echo "<div class=\"post_author\">\n";
                echo image("images/default_thumb.png", null, null, null, ".photo off")."\n";
                echo anchor("user.php?uid=".$message["from_name"], htmlspecialchars($message["from_name"]), "View ".$message["from_name"]."'s Profile", ".b")."<br />\n";
                echo "<span class=\"desc smaller\">".date("j F Y - h:i A", $message["send_time"])."</span>";
                echo "</div>\n";
                echo "<div class=\"post_content\">\n";
                echo $message["message"]."<br />\n";
                echo "</div>\n";
                echo "<div class=\"post_buttons small right\">\n";
                echo anchor("pm.php?action=delete&amp;pmid=".$pm_id, "Delete", "Delete PM")." - ".anchor("pm.php?action=reply&amp;pmid=".$pm_id."&amp;q=".$message["pm_id"], "Quote", "Quote");
                echo "</div>\n";
                echo "</div>\n";
            }
            echo "</div>\n";
            if ($messages_count > $config_message_replies_per_page)
            {
                echo "<div class=\"pagination\">\n";
                pagination("pm.php?action=read&amp;pmid=".$pm_id, $page, $pages);
                echo "</div>\n";
            }
        }
        else
        {
            echo "<div class=\"message error\">\n";
            echo "You can not read this message.<br />\n";
            echo "</div>\n";
        }
        
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("compose");
        echo "<div class=\"title\">Quick Reply</div>\n";
        echo "<form method=\"post\" action=\"pm.php?action=compose\">\n";
        echo "<div class=\"content\">\n";
        echo "<input type=\"hidden\" name=\"to\" id=\"to\" value=\"".$message["to_name"]."\" />\n";
        echo "<input type=\"hidden\" name=\"subject\" id=\"subject\" value=\"Re: ".$message["subject"]."\" />\n";
        echo "<textarea id=\"message\" name=\"message\" rows=\"5\" cols=\"40\" >".$message."</textarea><br /><br />\n";
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        echo "</div>\n";
        
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Post\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "</div>\n";
        echo "</form>\n";
        echo "<div class=\"content\">".image("images/comment_add.png")." ".anchor("pm.php?action=reply&amp;pmid=".$pm_id, "Add Reply", "Add new Reply", ".b")."</div>\n";
    }
    elseif ($action == "outbox")
    {
        echo "<div class=\"content\">";
        echo anchor("pm.php?action=compose", "Compose", "Compose", ".b")."<br />\n";
        echo anchor("pm.php", "Inbox", "Inbox", ".b")."<br />\n";
        echo "</div>\n";
        
        echo "<div class=\"list\">\n";
        echo "<div class=\"title\">Private Messages in Outbox</div>\n";
        
        $messages_count_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."private_messages WHERE  	from_id = '".$user["user_id"]."' AND reply_to = '0'");
        $messages_count = mysql_fetch_array($messages_count_query);
        $messages_count = $messages_count[0];
        $pages = ceil($messages_count / $config_messages_per_page);
        $count_start = ($page - 1) * $config_messages_per_page;
        $message_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."private_messages WHERE from_id = '".$user["user_id"]."' AND reply_to = '0' ORDER BY send_time DESC LIMIT ".$count_start.", ".$config_messages_per_page."");
        if ($messages_count > 0)
        {
            while ($message = mysql_fetch_array($message_query))
            {
                echo "<div class=\"row\">\n";
                echo "<div>\n";
                echo anchor("pm.php?action=read&amp;pmid=".$message["pm_id"], image("images/pm_read.png", "Sent message"), "Sent message")."\n";
                echo anchor("pm.php?action=read&amp;pmid=".$message["pm_id"], htmlspecialchars($message["subject"]), "View ".$message["subject"], "")."\n";
                
                echo "<div class=\"desc\" style=\"margin-left: 30px;\">To: ".anchor("user.php?uid=".$message["to_id"], htmlspecialchars($message["to_name"]), "View ".$message["from_name"]."'s Profile", ".b").", ".date("M j Y", $message["send_time"])."</div>\n";
                echo "</div>\n";
                echo "</div>\n";
            }
        }
        else
        {
            echo "<div class=\"row\">\n";
            echo "<div class=\"desc\">There are no private messages in this folder.</div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        if ($messages_count > $config_messages_per_page)
        {
            echo "<div class=\"pagination\">\n";
            pagination("pm.php", $page, $pages);
            echo "</div>\n";
        }
    }
    else
    {
        echo "<div class=\"content\">";
        echo anchor("pm.php?action=compose", "Compose", "Compose", ".b")."<br />\n";
        echo anchor("pm.php?action=outbox", "Outbox", "Outbox", ".b")."<br />\n";
        echo "</div>\n";
        
        echo "<div class=\"list\">\n";
        echo "<div class=\"title\">Private Messages in Inbox</div>\n";
        
        $messages_count_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."private_messages WHERE  	to_id = '".$user["user_id"]."' AND reply_to = '0'");
        $messages_count = mysql_fetch_array($messages_count_query);
        $messages_count = $messages_count[0];
        $pages = ceil($messages_count / $config_messages_per_page);
        $count_start = ($page - 1) * $config_messages_per_page;
        $message_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."private_messages WHERE to_id = '".$user["user_id"]."' AND reply_to = '0' ORDER BY send_time DESC LIMIT ".$count_start.", ".$config_messages_per_page."");
        if ($messages_count > 0)
        {
            while ($message = mysql_fetch_array($message_query))
            {
                if ($message["read_time"] == 0)
                {
                    echo "<div class=\"row odd\">\n";
                    $img = "pm_unread";
                    $img_alt = "Unread";
                    $link_bold = ".b";
                }
                else
                {
                    echo "<div class=\"row\">\n";
                    $img = "pm_read";
                    $img_alt = "Read";
                    $link_bold = "";
                }
                echo "<div>\n";
                echo anchor("pm.php?action=read&amp;pmid=".$message["pm_id"], image("images/".$img.".png", $img_alt), $img_alt." Message")."\n";
                echo anchor("pm.php?action=read&amp;pmid=".$message["pm_id"], htmlspecialchars($message["subject"]), "View ".$message["subject"],$link_bold)."\n";
                
                echo "<div class=\"desc\" style=\"margin-left: 30px;\">From: ".anchor("user.php?uid=".$message["from_id"], htmlspecialchars($message["from_name"]), "View ".$message["from_name"]."'s Profile", ".b").", ".date("M j Y", $message["send_time"])."</div>\n";
                echo "</div>\n";
                echo "</div>\n";
            }
        }
        else
        {
            echo "<div class=\"row\">\n";
            echo "<div class=\"desc\">There are no private messages in this folder.</div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        if ($messages_count > $config_messages_per_page)
        {
            echo "<div class=\"pagination\">\n";
            pagination("pm.php", $page, $pages);
            echo "</div>\n";
        }
    }
}
else
{
    login_form("You must log in to access this page", "notice");
}

include_once "themes/".$config_theme."/foot.php";
?>