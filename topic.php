<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";
if (isset($_GET["page"]))
{
    $page = (int)$_GET["page"];
}
else
{
    $page = 1;
}
mysql_query("UPDATE ".SQL_TABLE_PERFIX."topics SET views = views + 1 WHERE topic_id = '".$topic_id."'");
if (is_logged())
{
    $topic_user_map_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topic_user_map WHERE user_id = '".$user["user_id"]."' AND topic_id = '".$topic_id."'");
    if (mysql_numrows($topic_user_map_query) == 0)
    {
        mysql_query("INSERT INTO ".SQL_TABLE_PERFIX."topic_user_map (user_id, topic_id, topic_views, topic_forum_id, has_read, read_time) VALUES ('".$user["user_id"]."', '".$topic_id."', 1, '".$topic["parent_forum"]."', '1', '".$time."')");
    }
    else
    {
        mysql_query("UPDATE ".SQL_TABLE_PERFIX."topic_user_map SET has_read = '1', topic_views = topic_views + 1, read_time = '".$time."', topic_forum_id = '".$topic["topic_forum_id"]."' WHERE user_id = '".$user["user_id"]."' AND topic_id = '".$topic_id."'");
    }
}
echo "<div class=\"content\">".image("images/comment_add.png")." ".anchor("reply.php?tid=".$topic_id, "Add Reply", "Add new Reply", ".b")."</div>\n";
echo "<div class=\"title\">".anchor("topic.php?tid=".$topic_id, htmlspecialchars($topic["title"]))."</div>\n";
echo "<div class=\"posts\">\n";
$post_count_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."posts WHERE post_topic_id = '".$topic_id."'");
$post_count = mysql_fetch_array($post_count_query);
$post_count = $post_count[0];
$pages = ceil($post_count / $config_posts_per_page);
$count_start = ($page - 1) * $config_posts_per_page;
$post_start = $count_start + 1;
$post_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."posts WHERE post_topic_id = '".$topic_id."' ORDER BY post_id ASC LIMIT ".$count_start.", ".$config_posts_per_page."");
while ($post = mysql_fetch_array($post_query))
{
    $post["post_content"] = bbcode($post["post_content"]);
    if ($post["use_emo"] == 1)
    {
        $post["post_content"] = emoticons($post["post_content"]);
    }
    echo "<div class=\"post\">\n";
	echo "<div class=\"post_author\">\n";
	echo image("images/default_thumb.png", null, null, null, ".photo off")."\n";
    echo anchor("user.php?uid=".$post["poster_name"], htmlspecialchars($post["poster_name"]), "View ".$post["poster_name"]."'s Profile", ".b")."<br />\n";
	echo "<span class=\"desc smaller\">".date("j F Y - h:i A", $post["post_time"])."</span>";
	echo "</div>\n";
	echo "<div class=\"post_content\">\n";
	echo $post["post_content"]."<br />\n";
    echo "</div>\n";
    $attach_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments WHERE attach_post_id = '".$post["post_id"]."' ORDER BY attach_time ASC");
    if ((mysql_num_rows($attach_query)) > 0)
    {
        echo "<div class=\"post_attachments\">\n";
        echo "<b>Attached File(s):</b><br />\n";
        while ($attachment = mysql_fetch_array($attach_query))
        {
            $attach_type_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments_type WHERE atype_extension = '".$attachment["attach_file_ext"]."'");
            $attach_type = mysql_fetch_array($attach_type_query);
            echo anchor("attachment.php?aid=".$attachment["attach_id"], image($attach_type["atype_img"]), $attachment["attach_file"])." ".anchor("attachment.php?aid=".$attachment["attach_id"], htmlspecialchars($attachment["attach_file"]), $attachment["attach_file"])." <span class=\"desc\">(".file_size($attachment["attach_filesize"]).")</span><br />\n";
        }
        echo "</div>\n";
    }
    
    echo "<div class=\"post_buttons small right\">\n";
    if (is_logged())
    {
        if ($post["poster_name"] == $user["username"])
        {
            echo anchor("topic.php?tid=".$topic_id, "Delete", "Delete Reply")." - ".anchor("editpost.php?pid=".$post["post_id"], "Edit", "Edit Reply")." - ";
        }
    }
    echo anchor("reply.php?tid=".$topic_id."&amp;q=".$post["post_id"], "Quote", "Quote Reply");
    echo "</div>\n";
	echo "</div>\n";
}
echo "</div>\n";
if ($post_count > $config_posts_per_page)
{
    echo "<div class=\"pagination\">\n";
    pagination("topic.php?tid=".$topic_id, $page, $pages);
    echo "</div>\n";
}

echo "<div class=\"content\">".image("images/comment_add.png")." ".anchor("reply.php?tid=".$topic_id, "Add Reply", "Add new Reply", ".b")."</div>\n";
/*
$form_id = "wapbb".generate_form_id();
$_SESSION["form_id"] = $form_id;
$button_id = "wapbb".generate_button_id("reply");
echo "<form action=\"reply.php?tid=".$topic_id."\" method=\"post\">\n";
echo "<div class=\"content\">\n";
echo "<label for=\"post\">Quick Reply:</label><br />\n";
echo "<textarea rows=\"2\" name=\"post\" id=\"post\"></textarea>\n";
echo "<input type=\"hidden\" name=\"enable_emo\" id=\"enable_emo\" value=\"1\" />\n";
echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
echo "</div>\n";
echo "<div class=\"buttons\">\n";
echo "<input class=\"ibutton\" type=\"submit\" value=\"Post Reply\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
echo "</div>\n";
echo "</form>";
//*/

include_once "themes/".$config_theme."/foot.php";
?>