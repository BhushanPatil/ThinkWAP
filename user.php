<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

echo "<div class=\"content center\">";
echo image("images/default_thumb.png", $profile["username"]."'s Photo");
echo "</div>\n";

echo "<div class=\"title\"><b>".$profile["username"]."'s Forum Info</b></div>\n";

echo "<div class=\"content\">";

echo "<b>Joined:</b> ".date("j F Y", $profile["joined"])."<br />\n";

echo "<b>Active Posts:</b> ".$profile["num_posts"]."<br />\n";


echo "<b>Last Login:</b> ".date("M j Y h:i A", $profile["last_visit"])."<br />\n";

if (!empty($user["bday_day"]) && !empty($user["bday_month"]) && !empty($profile["bday_year"]))
{
    echo "<b>Age:</b> ".get_age($profile["bday_year"], $user["bday_month"], $profile["bday_day"])."<br />\n";
    echo "<b>Birthday:</b> ".$profile["bday_day"]."-".$user["bday_month"]."-".$profile["bday_year"]."<br />\n";
}
else
{
    echo "<b>Age:</b> Age Unknown<br />\n";
    echo "<b>Birthday:</b> Birthday Unknown<br />\n";
}


echo image("images/email.png")." ".anchor("pm.php?action=compose&amp;to=".$profile["username"], "Send PM to ".$profile["username"], "Send PM to ".$profile["username"], ".b")."<br />\n";


echo "</div>\n";

include_once "themes/".$config_theme."/foot.php";
?>