<?php
require_once "includes/start.php";
require_once "includes/functions.php";
if (isset($_GET["code"]))
{
    $get_code = $_GET["code"];
}
else
{
    exit;
}
$key = $_SESSION["captcha_key"];
$text = xoft_decode($get_code, $key);
$font = "fonts/coopbla.ttf";
$font_size = rand(18, 20);
$im = imagecreate(150, 40);
$angle = rand(-5, 5);
$color = imagecolorallocatealpha($im, rand(50, 150), rand(50, 150), rand(50, 150), 100);
$textsize = imagettfbbox($font_size, $angle, $font, $text);
$twidth = abs($textsize[2] - $textsize[0]);
$theight = abs($textsize[5] - $textsize[3]);
$x = (imagesx($im) / 2) - ($twidth / 2) + (rand(-20, 20));
$y = (imagesy($im)) - ($theight / 2);
imagettftext($im, $font_size, $angle, $x, $y, $color, $font, $text);
header("Content-Type: image/png");
imagepng($im, null, 9, PNG_NO_FILTER);
imagedestroy($im);
exit;
?>