<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

if (is_logged())
{
    echo "<div class=\"content\">";
    if ($action == "password")
    {
        echo anchor("usercp.php", "Account Summary", "Account Summary", ".b")."<br />\n";
        echo anchor("usercp.php?action=profile", "Edit Profile Information", "Edit Profile Information", ".b")."<br />\n";
        echo anchor("usercp.php?action=email", "Change Email Address", "Change Email Address", ".b")."<br />\n";
    }
    else if ($action == "email")
    {
        echo anchor("usercp.php", "Account Summary", "Account Summary", ".b")."<br />\n";
        echo anchor("usercp.php?action=profile", "Change Profile Information", "Change Profile Information", ".b")."<br />\n";
        echo anchor("usercp.php?action=password", "Change Password", "Change Password", ".b")."<br />\n";
    }
    else if ($action == "profile")
    {
        echo anchor("usercp.php", "Account Summary", "Account Summary", ".b")."<br />\n";
        echo anchor("usercp.php?action=password", "Change Password", "Change Password", ".b")."<br />\n";
        echo anchor("usercp.php?action=email", "Change Email Address", "Change Email Address", ".b")."<br />\n";
    }
    else
    {
        echo anchor("usercp.php?action=profile", "Edit Profile Information", "Edit Profile Information", ".b")."<br />\n";
        echo anchor("usercp.php?action=password", "Change Password", "Change Password", ".b")."<br />\n";
        echo anchor("usercp.php?action=email", "Change Email Address", "Change Email Address", ".b")."<br />\n";
    }
    
    echo "</div>\n";
    
    if ($action == "password")
    {
        echo "<div class=\"title\">Change Password</div>\n";
        $error = array();
        if (isset($_SESSION["form_id"]))
        {
            $old_form_id = $_SESSION["form_id"];
        }
        else
        {
            $old_form_id = "";
        }
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("password");
        if (isset($_POST[$button_id]))
        {
            if ($old_form_id != $_POST["form_id"])
            {
                $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
            }
            
            $old_password = $_POST["old_password"];
            $old_password = strip_tags($old_password);
            
            $password = $_POST["password"];
            $password = strip_tags($password);
            
            $password2 = $_POST["password2"];
            $password2 = strip_tags($password2);
            
            $old_password_hash = password_hash($user["password_salt"], $old_password);
            if ($old_password_hash != $user["password_hash"])
            {
                $error["password"] = "The current password you entered is incorrect";
            }
            elseif (strlen($password) > 32 || strlen($password) < 3)
            {
                $error["password"] = "Password must be at least 3 characters in length and at most 32";
            }
            elseif ($password != $password2)
            {
                $error["password"] = "The password fields did not match";
            }
            
            if (empty($error))
            {
                echo "<div class=\"content\">\n";
                echo "<div class=\"message\">\n";
                
                $password_salt = passsword_salt();
                $password_hash = password_hash($password_salt, $password);
                
                mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET password_hash = '".$password_hash."', password_salt = '".$password_salt."' WHERE user_id = '".$user["user_id"]."'");
                
                echo "<b>Your password has successfully been updated</b><br />\n";
                echo "</div>\n";
                echo "</div>\n";
            }
        }
        echo "<form method=\"post\" action=\"usercp.php?action=password\">\n";
        echo "<div class=\"content\">\n";
        
        if (!empty($error["form"]))
        {
            echo "<div class=\"message error\">".$error["form"]."</div>\n";
        }
        elseif (!empty($error["password"]))
        {
            echo "<div class=\"message error\">".$error["password"]."</div>\n";
        }

        echo "<label for=\"old_password\">Current Password:</label><br />\n";
        echo "<input type=\"password\" id=\"old_password\" name=\"old_password\" value=\"\" maxlength=\"32\" /><br /><br />\n";
        
        echo "<label for=\"password\">New Password:</label><br />\n";
        echo "<input type=\"password\" id=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br /><br />\n";
        
        echo "<label for=\"password2\">Confirm Password::</label><br />\n";
        echo "<input type=\"password\" id=\"password2\" name=\"password2\" value=\"\" maxlength=\"32\" /><br /><br />\n";
        
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        
        echo "</div>\n";
        
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Update Password\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "</div>\n";
        
        echo "</form>\n";
    }
    else if ($action == "email")
    {
        echo "<div class=\"title\">Change Email Address</div>\n";
        $error = array();
        if (isset($_SESSION["form_id"]))
        {
            $old_form_id = $_SESSION["form_id"];
        }
        else
        {
            $old_form_id = "";
        }
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("email");
        if (isset($_POST[$button_id]))
        {
            if ($old_form_id != $_POST["form_id"])
            {
                $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
            }
            
            $password = $_POST["password"];
            $password = strip_tags($password);
            
            $email = $_POST["email"];
            $email = strip_tags($email);
            
            $email2 = $_POST["email2"];
            $email2 = strip_tags($email2);
            
            $password_hash = password_hash($user["password_salt"], $password);
            if ($password_hash != $user["password_hash"])
            {
                $error["password"] = "The password you entered is incorrect";
            }
            
            if ($email != "")
            {
                if ($email == $email2)
                {
                    if (check_email($email) == true)
                    {
                        $email_registered_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE email = '".$email."' LIMIT 1");
                        if (mysql_num_rows($email_registered_query) >= 1)
                        {
                            $error["email"] = "Already someone registered with email address you entered";
                        }
                    }
                    else
                    {
                        $error["email"] = "The email address you entered is invalid (ex: name@domain.com)";
                    }
                }
                else
                {
                    $error["email"] = "The email address you entered as confirmation does not match the original one. Please confirm your email address correctly.";
                }
            }
            else
            {
                $error["email"] = "You did not enter an email address. Please enter one.";
            }
            
            if (empty($error))
            {
                echo "<div class=\"content\">\n";
                echo "<div class=\"message\">\n";
                
                mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET prev_email = email, email = '".$email."' WHERE user_id = '".$user["user_id"]."'");
                
                echo "<b>Your email address has successfully been updated</b><br />\n";
                echo "</div>\n";
                echo "</div>\n";
            }
        }
        
        echo "<form method=\"post\" action=\"usercp.php?action=email\">\n";
        echo "<div class=\"content\">\n";
        
        if (!empty($error["form"]))
        {
            echo "<div class=\"message error\">".$error["form"]."</div>\n";
        }
        elseif (!empty($error["password"]))
        {
            echo "<div class=\"message error\">".$error["password"]."</div>\n";
        }
        elseif (!empty($error["email"]))
        {
            echo "<div class=\"message error\">".$error["email"]."</div>\n";
        }
        
        echo "<label for=\"password\">Please Enter Your Password:</label><br />\n";
        echo "<input type=\"password\" id=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br /><br />\n";
        
        echo "<label for=\"email\">New Email Address:</label><br />\n";
        echo "<input type=\"text\" id=\"email\" name=\"email\" value=\""."\" maxlength=\"150\" /><br /><br />\n";
        
        echo "<label for=\"email2\">Confirm Email Address:</label><br />\n";
        echo "<input type=\"text\" id=\"email2\" name=\"email2\" value=\""."\" maxlength=\"150\" /><br />\n";
        
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        
        echo "</div>\n";
        
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Update Email Address\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "</div>\n";
        
        echo "</form>\n";
        
    }
    else if ($action == "profile")
    {
        echo "<div class=\"title\">Edit Profile Information</div>\n";
        
        $error = array();
        if (isset($_SESSION["form_id"]))
        {
            $old_form_id = $_SESSION["form_id"];
        }
        else
        {
            $old_form_id = "";
        }
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("pi");
        if (isset($_POST[$button_id]))
        {
            if ($old_form_id != $_POST["form_id"])
            {
                $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
            }
            
            $first_name = $_POST["first_name"];
            $first_name = strip_tags($first_name);
            
            $last_name = $_POST["last_name"];
            $last_name = strip_tags($last_name);
            
            $location = $_POST["location"];
            $location = strip_tags($location);
            
            $gender = $_POST["gender"];
            $gender = strip_tags($gender);
            
            $bday_day = $_POST["bday_day"];
            $bday_day = strip_tags($bday_day);
            
            $bday_month = $_POST["bday_month"];
            $bday_month = strip_tags($bday_month);
            
            $bday_year = $_POST["bday_year"];
            $bday_year = strip_tags($bday_year);
            
            $site_url = $_POST["site_url"];
            $site_url = strip_tags($site_url);
            
            if (empty($error))
            {
                echo "<div class=\"content\">\n";
                echo "<div class=\"message\">\n";
                mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET first_name = '".$first_name."', last_name = '".$last_name."', loaction = '".$location."', gender = '".$gender."', bday_day = '".$bday_day."', bday_month = '".$bday_month."', bday_year = '".$bday_year."', site_url = '".$site_url."' WHERE user_id = '".$user["user_id"]."'");
                echo "<b>Profile information saved successfully!</b><br />\n";
                $user = get_user($_SESSION["username"]);
                echo "</div>\n";
                echo "</div>\n";
            }
        }
        echo "<form method=\"post\" action=\"usercp.php\">\n";
        echo "<div class=\"content\">\n";
        
        if (!empty($error["form"]))
        {
            echo "<div class=\"message error\">\n";
            echo $error["form"]."<br />\n";
            echo "</div>\n";
        }
        
        echo "<label for=\"first_name\">First Name:</label><br />\n";
        echo "<input type=\"text\" id=\"first_name\" name=\"first_name\" value=\"".$user["first_name"]."\" maxlength=\"50\" /><br /><br />\n";
        
        echo "<label for=\"last_name\">Last Name:</label><br />\n";
        echo "<input type=\"text\" id=\"last_name\" name=\"last_name\" value=\"".$user["last_name"]."\" maxlength=\"50\" /><br /><br />\n";
        
        echo "<label for=\"location\">Location:</label><br />\n";
        echo "<input type=\"text\" id=\"location\" name=\"location\" value=\"".$user["loaction"]."\" maxlength=\"50\" /><br /><br />\n";
        
        echo "<label for=\"gender\">Gender:</label><br />\n";
        echo "<select name=\"gender\" id=\"gender\">\n";
        
        if ($user["gender"] == 0)
        {
            echo "<option value=\"0\" selected=\"selected\">Not Telling</option>\n";
        }
        else
        {
            echo "<option value=\"0\">Not Telling</option>\n";
        }
        if ($user["gender"] == 1)
        {
            echo "<option value=\"1\" selected=\"selected\">Male</option>\n";
        }
        else
        {
            echo "<option value=\"1\">Male</option>\n";
        }
        if ($user["gender"] == 2)
        {
            echo "<option value=\"2\" selected=\"selected\">Female</option>\n";
        }
        else
        {
            echo "<option value=\"2\">Female</option>\n";
        }
        
        if ($user["gender"] == 3)
        {
            echo "<option value=\"3\" selected=\"selected\">Other</option>\n";
        }
        else
        {
            echo "<option value=\"3\">Other</option>\n";
        }
        
        echo "</select><br /><br />\n";
        
        echo "<label for=\"bday_day\">Date of Birth::</label><br />\n";
        echo "<select name=\"bday_day\" id=\"bday_day\">\n";
        echo "<option value=\"0\">--</option>\n";
        for ($i = 1; $i <= 31; $i++)
        {
            if ($user["bday_day"] == $i)
            {
                echo "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n";
            }
            else
            {
                echo "<option value=\"".$i."\">".$i."</option>\n";
            }
        }
        echo "</select>\n";
        echo "<select name=\"bday_month\" id=\"bday_month\">\n";
        echo "<option value=\"0\">--</option>\n";
        for ($i = 1; $i <= 12; $i++)
        {
            if ($user["bday_month"] == $i)
            {
                echo "<option value=\"".$i."\" selected=\"selected\">".date("F",  mktime(0, 0, 0, $i))."</option>\n";
            }
            else
            {
                echo "<option value=\"".$i."\">".date("F",  mktime(0, 0, 0, $i))."</option>\n";
            }
        }
        echo "</select>\n";
        echo "<select name=\"bday_year\" id=\"bday_year\">\n";
        echo "<option value=\"0\">--</option>\n";
        for ($i = date("Y"); $i >= 1900; $i--)
        {
            if ($user["bday_year"] == $i)
            {
                echo "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n";
            }
            else
            {
                echo "<option value=\"".$i."\">".$i."</option>\n";
            }
        }
        echo "</select><br /><br />\n";
        echo "<label for=\"site_url\">Your Website URL::</label><br />\n";
        echo "<input type=\"text\" id=\"site_url\" name=\"site_url\" value=\"".$user["site_url"]."\" maxlength=\"50\" /><br /><br />\n";
        
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        echo "</div>\n";
        
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Save Changes\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "</div>\n";
        echo "</form>\n";
    }
    else
    {
        echo "<div class=\"title\">Your Account Summary</div>\n";
        echo "<div class=\"content\">\n";
        echo "<div class=\"row\">Username: ".$user["username"]."</div>\n";
        echo "<div class=\"row\">Posts: ".$user["num_posts"]."</div>\n";
        echo "<div class=\"row\">Email Address: ".$user["email"]."</div>\n";
        echo "<div class=\"row\">Registration Date: ".date("d-m-Y, h:i A", $user["last_visit"])."</div>\n";
        echo "</div>\n";
    }
}
else
{
    login_form("You must log in to access UCP", "notice");
}

include_once "themes/".$config_theme."/foot.php";
?>