<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";
if (isset($_GET["page"]))
{
    $page = (int)$_GET["page"];
}
else
{
    $page = 1;
}

if (isset($_GET["q"]) && !empty($_GET["q"]))
{
    $query = $_GET["q"];
}
elseif (isset($_POST["q"]) && !empty($_POST["q"]))
{
    $query = $_POST["q"];
}
else
{
    $query = "";
}

$button_id = "wapbb".generate_button_id("search");
if (isset($_POST[$button_id]) || $query != "")
{
    $error = array();
    
    if ($_SESSION["form_id"] != $_POST["form_id"])
    {
        $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
    }
    
    if ($query == "")
    {
        $error["query"] = "Enter your search keywords";
    }
    elseif (strlen($query) > 50)
    {
        $error["query"] = "The search keywords must be below 50 characters";
    }
    else
    {
        echo "<div class=\"list\">\n";
        echo "<div class=\"title\">Forum Search</div>\n";
        
        if (isset($_POST["forum"]))
        {
            $forum_id = abs(intval($_POST["forum"]));
            $forum = get_forum($forum_id);
            if ($forum != false)
            {
                if ($forum["parent_id"] == 0)
                {
                    $find_in_forum = "AND (";
                    $sub_forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$forum_id."'");
                    $i = 1;
                    while ($sub_forum = mysql_fetch_array($sub_forum_query))
                    {
                        if ($i == 1)
                        {
                            $find_in_forum .= "topic_forum_id = '".$sub_forum["forum_id"]."'";
                        }
                        else
                        {
                            $find_in_forum .= "OR topic_forum_id = '".$sub_forum["forum_id"]."'";
                        }
                        $i++;
                    }
                    $find_in_forum .= ")";
                }
                else
                {
                    $find_in_forum = "AND topic_forum_id = '".$_POST["forum"]."'";
                }
            }
            else
            {
                $find_in_forum = "";
            }
        }
        
        $where = $_POST["where"];
        if ($where == 1)
        {
            $result_count_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics WHERE title LIKE '%".$query."%' ".$find_in_forum);
            $result_count = mysql_num_rows($result_count_query);
            $pages = ceil($result_count / $config_topics_per_page);
            $count_start = ($page - 1) * $config_topics_per_page;
            $result_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics WHERE title LIKE '%".$query."%' ".$find_in_forum." ORDER BY last_post_time DESC LIMIT ".$count_start.", ".$config_topics_per_page."");
        }
        elseif ($where == 2)
        {
            $result_count_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics, ".SQL_TABLE_PERFIX."posts WHERE (".SQL_TABLE_PERFIX."topics.topic_id = ".SQL_TABLE_PERFIX."posts.post_topic_id) AND post_content LIKE '%".$query."%' ".$find_in_forum." GROUP BY post_topic_id");
            $result_count = mysql_num_rows($result_count_query);
            $pages = ceil($result_count / $config_topics_per_page);
            $count_start = ($page - 1) * $config_topics_per_page;
            $result_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics, ".SQL_TABLE_PERFIX."posts WHERE (".SQL_TABLE_PERFIX."topics.topic_id = ".SQL_TABLE_PERFIX."posts.post_topic_id) AND post_content LIKE '%".$query."%' ".$find_in_forum." GROUP BY post_topic_id ORDER BY post_time DESC LIMIT ".$count_start.", ".$config_topics_per_page."");
        }
        else
        {
            $result_count_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics, ".SQL_TABLE_PERFIX."posts WHERE (".SQL_TABLE_PERFIX."topics.topic_id = ".SQL_TABLE_PERFIX."posts.post_topic_id) AND (title LIKE '%".$query."%' OR post_content LIKE '%".$query."%') ".$find_in_forum." GROUP BY post_topic_id");
            $result_count = mysql_num_rows($result_count_query);
            $pages = ceil($result_count / $config_topics_per_page);
            $count_start = ($page - 1) * $config_topics_per_page;
            $result_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics, ".SQL_TABLE_PERFIX."posts WHERE (".SQL_TABLE_PERFIX."topics.topic_id = ".SQL_TABLE_PERFIX."posts.post_topic_id) AND (title LIKE '%".$query."%' OR post_content LIKE '%".$query."%') ".$find_in_forum." GROUP BY post_topic_id ORDER BY post_time DESC LIMIT ".$count_start.", ".$config_topics_per_page."");
        }
        if ($result_count >= 0)
        {
            echo "<div class=\"row\">\n";
            echo "Your search for the term <b>".htmlspecialchars($query)."</b> returned ".$result_count." results";
            echo "</div>\n";
            while ($result = mysql_fetch_array($result_query))
            {
                echo "<div class=\"row\">\n";
                echo "<div>\n";
                $img = "topic";
                if ($result["closed"] == 0)
                {
                    if ($result["num_posts"] > 20)
                    {
                        $img .= "_hot";
                    }
                    if (is_logged())
                    {
                        $is_user_reply_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."posts WHERE poster_id = '".$user["user_id"]."' AND post_topic_id = '".$result["topic_id"]."'");
                        if (mysql_numrows($is_user_reply_query) >= 1)
                        {
                            $img .= "_dot";
                        }
                        $topic_user_map_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topic_user_map WHERE user_id = '".$user["user_id"]."' AND topic_id = '".$result["topic_id"]."'");
                        if (mysql_numrows($topic_user_map_query) == 0)
                        {
                            $img .= "_unread";
                            $img_alt = "Unread";
                        }
                        else
                        {
                            $topic_user_map = mysql_fetch_array($topic_user_map_query);
                            if ($topic_user_map["has_read"] == 0)
                            {
                                $img .= "_unread";
                                $img_alt = "Unread";
                            }
                            else
                            {
                                $img .= "_read";
                                $img_alt = "Read";
                            }
                        }
                    }
                    else
                    {
                        $img .= "_unread";
                        $img_alt = "Unread";
                    }
                }
                else
                {
                    $img .= "_closed";
                    $img_alt = "Closed";
                }
                echo anchor("topic.php?tid=".$result["topic_id"], image("images/".$img.".png", $img_alt), "Unread Topic")."\n";
                
                echo anchor("topic.php?tid=".$result["topic_id"], htmlspecialchars($result["title"]), "View ".$result["title"], ".b")."\n";
                echo "</div>\n";
                if ($result["num_posts"] > 0)
                {
                    echo "<div class=\"desc\" style=\"margin-left: 30px;\">".$result["num_posts"]." Replies: Last by ".$result["last_poster_name"].", ".date("M j Y h:i A", $result["last_post_time"])."</div>\n";
                }
                else
                {
                    echo "<div class=\"desc\" style=\"margin-left: 30px;\">0 Replies: Last by -</div>\n";
                }
                echo "</div>\n";
            }
        }
        else
        {
            echo "<div class=\"row\">\n";
            echo "<div class=\"desc\">No topics were found</div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        include_once "themes/".$config_theme."/foot.php";
    }
}

echo "<form action=\"search.php\" method=\"POST\">\n";

echo "<div class=\"content\">\n";

if (!empty($error["form"]))
{
    echo "<div class=\"message error\">".$error["form"]."</div>\n";
}
elseif (!empty($error["query"]))
{
    echo "<div class=\"message error\">".$error["query"]."</div>\n";
}

echo "<label for=\"q\">Find words:</label><br />\n";
echo "<input type=\"text\" name=\"q\" id=\"q\" /><br /><br />\n";

echo "<label for=\"q\">Find in forum:</label><br />\n";
echo "<select name=\"forum\" id=\"forum\">\n";

echo "<option value=\"0\" selected=\"selected\">All</option>\n";
$forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '0' ORDER BY forum_position ASC");
while ($forum = mysql_fetch_array($forum_query))
{
    echo "<option value=\"".$forum["forum_id"]."\">".htmlspecialchars($forum["forum_name"])."</option>\n";
    $sub_forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$forum["forum_id"]."' ORDER BY forum_position ASC");
    while ($sub_forum = mysql_fetch_array($sub_forum_query))
    {
        echo "<option value=\"".$sub_forum["forum_id"]."\">&nbsp;-&nbsp;".htmlspecialchars($sub_forum["forum_name"])."</option>\n";
    }
}
echo "</select><br /><br />\n";

echo "<label for=\"where\">Find in:</label><br />\n";
echo "<select name=\"where\" id=\"where\">\n";
echo "<option value=\"0\" selected=\"selected\">Topic titles &amp; Posts</option>\n";
echo "<option value=\"1\">Topic titles only</option>\n";
echo "<option value=\"2\">Posts only</option>\n";
echo "</select><br /><br />\n";

$form_id = "wapbb".generate_form_id();
$_SESSION["form_id"] = $form_id;
echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";

echo "</div>\n";

echo "<div class=\"buttons\">\n";
echo "<input class=\"button ibutton\" type=\"submit\" value=\"Search Now\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
echo "or ".anchor(SITE_URL, "Cancel", "Cancel search")."\n";
echo "</div>\n";
echo "</form>\n";

include_once "themes/".$config_theme."/foot.php";
?>