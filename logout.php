<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
$domain = get_host(SITE_URL);
$secure = isset($_SERVER["HTTPS"]);
if(isset($_COOKIE["log_user"]))
{
    setcookie("log_user", "", $time - 3600, "/", $domain, $secure, true);
    unset($_COOKIE["log_user"]);
}
if(isset($_COOKIE["log_pass"]))
{
    setcookie("log_pass", "", $time - 3600, "/", $domain, $secure, true);
    unset($_COOKIE["log_pass"]);
}
if (isset($_SESSION["username"]))
{
    $_SESSION["username"] = "";
    unset($_SESSION["username"]);
}
session_unset();
session_destroy();
header("Location: ".SITE_URL);
exit;
?>