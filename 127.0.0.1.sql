-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2013 at 03:19 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thinkwap`
--
CREATE DATABASE `thinkwap` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `thinkwap`;

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_attachments`
--

CREATE TABLE IF NOT EXISTS `wapbb_attachments` (
  `attach_id` int(10) NOT NULL AUTO_INCREMENT,
  `attach_file` varchar(250) NOT NULL DEFAULT '',
  `attach_file_ext` varchar(10) NOT NULL DEFAULT '',
  `attach_location` varchar(250) NOT NULL DEFAULT '',
  `attach_filesize` int(10) NOT NULL DEFAULT '0',
  `attach_time` int(10) NOT NULL DEFAULT '0',
  `attach_hits` int(10) NOT NULL DEFAULT '0',
  `attach_post_id` int(10) NOT NULL DEFAULT '0',
  `attach_user_id` int(8) NOT NULL DEFAULT '0',
  `attach_user_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`attach_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wapbb_attachments`
--

INSERT INTO `wapbb_attachments` (`attach_id`, `attach_file`, `attach_file_ext`, `attach_location`, `attach_filesize`, `attach_time`, `attach_hits`, `attach_post_id`, `attach_user_id`, `attach_user_name`) VALUES
(1, 'Test.txt', 'txt', 'attachments/15623496154d3c52caedaa02.07073329', 0, 1295798986, 0, 1, 1, 'BHUSHAN');

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_attachments_type`
--

CREATE TABLE IF NOT EXISTS `wapbb_attachments_type` (
  `atype_id` int(10) NOT NULL AUTO_INCREMENT,
  `atype_extension` varchar(18) NOT NULL DEFAULT '',
  `atype_mimetype` varchar(255) NOT NULL DEFAULT '',
  `atype_post` tinyint(1) NOT NULL DEFAULT '1',
  `atype_photo` tinyint(1) NOT NULL DEFAULT '0',
  `atype_img` text,
  PRIMARY KEY (`atype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `wapbb_attachments_type`
--

INSERT INTO `wapbb_attachments_type` (`atype_id`, `atype_extension`, `atype_mimetype`, `atype_post`, `atype_photo`, `atype_img`) VALUES
(1, 'bmp', 'image/x-MS-bmp', 1, 0, '.images/mime_types/gif.gif'),
(2, 'css', 'text/css', 1, 0, 'images/mime_types/script.gif'),
(3, 'doc', 'application/msword', 1, 0, 'images/mime_types/doc.gif'),
(4, 'gif', 'image/gif', 1, 1, 'images/mime_types/gif.gif'),
(5, 'gz', 'application/x-gzip', 1, 0, 'images/mime_types/zip.gif'),
(6, 'hqx', 'application/mac-binhex40', 1, 0, 'images/mime_types/stuffit.gif'),
(7, 'ico', 'image/x-icon', 1, 0, 'images/mime_types/gif.gif'),
(8, 'jpeg', 'image/jpeg', 1, 1, 'images/mime_types/gif.gif'),
(9, 'jpg', 'image/jpeg', 1, 1, 'images/mime_types/gif.gif'),
(10, 'pdf', 'application/pdf', 1, 0, 'images/mime_types/pdf.gif'),
(11, 'png', 'image/png', 1, 1, 'images/mime_types/gif.gif'),
(12, 'ppt', 'application/vnd.ms-powerpoint', 1, 0, 'images/mime_types/ppt.gif'),
(13, 'rtf', 'application/msword', 1, 0, 'images/mime_types/rtf.gif'),
(14, 'swf', 'application/x-shockwave-flash', 0, 0, 'images/mime_types/quicktime.gif'),
(15, 'tar', 'application/x-tar', 1, 0, 'images/mime_types/zip.gif'),
(16, 'txt', 'text/plain', 1, 0, 'images/mime_types/txt.gif'),
(17, 'wbmp', 'image/vnd.wap.wbmp', 1, 1, 'images/mime_types/gif.gif'),
(18, 'xml', 'application/xml', 1, 0, 'images/mime_types/script.gif'),
(19, 'zip', 'application/zip', 1, 0, 'images/mime_types/zip.gif'),
(20, 'php', 'text/x-php', 1, 0, 'images/mime_types/php.gif'),
(21, 'rar', 'application/x-rar-compressed', 1, 0, 'images/mime_types/zip.gif');

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_banned`
--

CREATE TABLE IF NOT EXISTS `wapbb_banned` (
  `ban_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `content` text NOT NULL,
  `time` int(10) NOT NULL DEFAULT '0',
  `lifted` int(10) NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL DEFAULT 'Unknown',
  PRIMARY KEY (`ban_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_config_settings`
--

CREATE TABLE IF NOT EXISTS `wapbb_config_settings` (
  `config_title` varchar(250) NOT NULL,
  `config_value` text,
  PRIMARY KEY (`config_title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_forums`
--

CREATE TABLE IF NOT EXISTS `wapbb_forums` (
  `forum_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) NOT NULL DEFAULT '0',
  `forum_name` varchar(128) NOT NULL DEFAULT '',
  `forum_description` text,
  `forum_position` int(5) DEFAULT '0',
  `num_topics` mediumint(6) NOT NULL DEFAULT '0',
  `num_posts` mediumint(6) NOT NULL DEFAULT '0',
  `last_post_id` int(10) NOT NULL DEFAULT '0',
  `last_post_time` int(10) NOT NULL DEFAULT '0',
  `last_poster_id` mediumint(8) NOT NULL DEFAULT '0',
  `last_poster_name` varchar(50) NOT NULL DEFAULT '',
  `min_posts_view` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`forum_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `wapbb_forums`
--

INSERT INTO `wapbb_forums` (`forum_id`, `parent_id`, `forum_name`, `forum_description`, `forum_position`, `num_topics`, `num_posts`, `last_post_id`, `last_post_time`, `last_poster_id`, `last_poster_name`, `min_posts_view`) VALUES
(1, 0, 'Site Information News', NULL, 1, 0, 0, 0, 0, 0, '', 0),
(2, 1, 'News and Information', 'All news and announcements for the forums will be posted here.', 1, 4, 15, 17, 1336226136, 1, 'BHUSHAN', 0),
(3, 1, 'The Forums', 'Forum introductions, competitions and suggestions.', 2, 0, 0, 0, 0, 0, '', 0),
(4, 3, 'Introductions', 'Introduce yourself to the community here!', 1, 0, 0, 0, 0, 0, '', 0),
(5, 3, 'Competitions', 'Official forum competitions will be posted here.', 2, 0, 0, 0, 0, 0, '', 0),
(6, 3, 'Suggestions', 'Post your suggestions or comments here.', 3, 0, 0, 0, 0, 0, '', 0),
(7, 0, 'Wap Scripts', NULL, 2, 0, 0, 0, 0, 0, '', 0),
(8, 7, 'Chat/Forum Scripts', 'Use this board to share Chat and Forum scripts.', 1, 0, 0, 0, 0, 0, '', 0),
(9, 7, 'Social Community/Dating Scripts', 'Use this board to share Social Community and Dating scripts.', 2, 0, 0, 0, 0, 0, '', 0),
(10, 7, 'Download/Upload Scripts', 'Use this board to share Download and Upload scripts', 3, 0, 0, 0, 0, 0, '', 0),
(11, 7, 'Toplist/Directory Scripts', 'Use this board to share Toplist and Directory scripts.', 4, 0, 0, 0, 0, 0, '', 0),
(12, 7, 'Online Games/Fun Scripts', 'Use this board to share Online Games and Fun scripts.', 5, 0, 0, 0, 0, 0, '', 0),
(13, 7, 'Other Scripts', 'Use this board to share scripts that doesn''t fit in the above boards.', 6, 0, 0, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_posts`
--

CREATE TABLE IF NOT EXISTS `wapbb_posts` (
  `post_id` int(10) NOT NULL AUTO_INCREMENT,
  `post_content` mediumtext NOT NULL,
  `post_topic_id` int(10) NOT NULL,
  `post_time` int(10) NOT NULL,
  `poster_id` mediumint(8) NOT NULL,
  `poster_name` varchar(50) NOT NULL,
  `use_emo` tinyint(1) NOT NULL DEFAULT '1',
  `use_sig` tinyint(1) NOT NULL DEFAULT '1',
  `edit_time` int(10) NOT NULL DEFAULT '0',
  `edit_reason` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `wapbb_posts`
--

INSERT INTO `wapbb_posts` (`post_id`, `post_content`, `post_topic_id`, `post_time`, `poster_id`, `poster_name`, `use_emo`, `use_sig`, `edit_time`, `edit_reason`) VALUES
(1, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295798986, 1, 'BHUSHAN', 1, 0, 1296069517, ''),
(2, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295801326, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(3, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295801423, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(4, 'TEST\r\nTEST\r\nTEST\r\nTEST', 2, 1295896006, 1, 'BHUSHAN', 1, 0, 1296069688, ''),
(9, 'thanks', 1, 1300213368, 1, 'BHUSHAN', 1, 1, 0, ''),
(5, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295968790, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(6, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295985043, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(7, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 1, 1295985047, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(8, '[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]', 2, 1296025156, 1, 'BHUSHAN', 1, 1, 1296069517, ''),
(10, 'test', 1, 1302206381, 1, 'BHUSHAN', 1, 1, 0, ''),
(11, 'dfgdfg', 1, 1302206603, 1, 'BHUSHAN', 1, 1, 0, ''),
(12, 'r', 1, 1302206692, 1, 'BHUSHAN', 1, 1, 0, ''),
(13, 'test', 1, 1302206739, 1, 'BHUSHAN', 1, 1, 0, ''),
(14, 'fsdhg', 1, 1302206834, 1, 'BHUSHAN', 1, 1, 0, ''),
(15, 'a', 3, 1336139587, 1, 'BHUSHAN', 1, 0, 0, ''),
(16, 'testtest', 4, 1336140071, 1, 'BHUSHAN', 1, 0, 0, ''),
(17, '[quote=''BHUSHAN'' pid=''8'' time=''1296025156'']\r\n[b]Post[/b]\r\n[i]Post[/i]\r\n[u]Post[/u]\r\n\r\n[code=php]\r\n<?php\r\necho "test";\r\n?>\r\n[/code]\r\n[/quote]\r\nLOL :)', 2, 1336226136, 1, 'BHUSHAN', 1, 1, 1336226151, '');

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_private_messages`
--

CREATE TABLE IF NOT EXISTS `wapbb_private_messages` (
  `pm_id` int(10) NOT NULL AUTO_INCREMENT,
  `from_id` int(10) NOT NULL,
  `from_name` varchar(50) NOT NULL,
  `to_id` int(10) NOT NULL,
  `to_name` varchar(50) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL,
  `send_time` int(10) NOT NULL,
  `read_time` int(10) NOT NULL,
  `reply_to` int(10) NOT NULL DEFAULT '0',
  `read_receipt` tinyint(1) NOT NULL DEFAULT '0',
  `use_emo` tinyint(1) NOT NULL DEFAULT '1',
  `use_sig` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wapbb_private_messages`
--

INSERT INTO `wapbb_private_messages` (`pm_id`, `from_id`, `from_name`, `to_id`, `to_name`, `subject`, `message`, `send_time`, `read_time`, `reply_to`, `read_receipt`, `use_emo`, `use_sig`) VALUES
(1, 2, 'AKON', 1, 'BHUSHAN', 'Test', 'Test :P', 1336227573, 0, 0, 0, 1, 0),
(2, 2, 'AKON', 1, 'BHUSHAN', 'TestTest', 'TestTest :(', 1336227724, 0, 0, 0, 1, 0),
(3, 2, 'AKON', 1, 'BHUSHAN', 'TestTestTest', 'TestTestTest', 1336227780, 0, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_settings`
--

CREATE TABLE IF NOT EXISTS `wapbb_settings` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL DEFAULT '',
  `Value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_topic_user_map`
--

CREATE TABLE IF NOT EXISTS `wapbb_topic_user_map` (
  `user_id` int(10) NOT NULL DEFAULT '0',
  `topic_id` int(10) NOT NULL DEFAULT '0',
  `topic_views` int(10) NOT NULL DEFAULT '0',
  `topic_forum_id` smallint(5) NOT NULL DEFAULT '0',
  `has_read` int(1) NOT NULL DEFAULT '0',
  `read_time` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wapbb_topic_user_map`
--

INSERT INTO `wapbb_topic_user_map` (`user_id`, `topic_id`, `topic_views`, `topic_forum_id`, `has_read`, `read_time`) VALUES
(1, 3, 6, 2, 1, 1336140646),
(1, 2, 134, 2, 1, 1336330453),
(1, 1, 435, 2, 1, 1336331119),
(1, 2, 133, 2, 1, 1336330453),
(1, 3, 2, 2, 1, 1336140646),
(1, 4, 2, 2, 1, 1336146669),
(2, 2, 5, 2, 1, 1336229988),
(2, 4, 1, 0, 1, 1336230001),
(2, 1, 23, 2, 1, 1336230157);

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_topics`
--

CREATE TABLE IF NOT EXISTS `wapbb_topics` (
  `topic_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `topic_forum_id` smallint(10) NOT NULL,
  `start_time` int(10) NOT NULL,
  `starter_id` mediumint(8) NOT NULL,
  `starter_name` varchar(50) NOT NULL,
  `last_post_id` int(10) NOT NULL DEFAULT '0',
  `last_post_time` int(10) NOT NULL DEFAULT '0',
  `last_poster_id` mediumint(8) NOT NULL DEFAULT '0',
  `last_poster_name` varchar(50) NOT NULL,
  `num_posts` int(10) NOT NULL DEFAULT '0',
  `views` int(10) NOT NULL DEFAULT '0',
  `has_attach` tinyint(1) NOT NULL DEFAULT '0',
  `pinned` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `wapbb_topics`
--

INSERT INTO `wapbb_topics` (`topic_id`, `title`, `description`, `topic_forum_id`, `start_time`, `starter_id`, `starter_name`, `last_post_id`, `last_post_time`, `last_poster_id`, `last_poster_name`, `num_posts`, `views`, `has_attach`, `pinned`, `approved`, `closed`) VALUES
(1, 'TEST', 'TEST', 2, 1295798986, 1, 'BHUSHAN', 14, 1302206834, 1, 'BHUSHAN', 11, 489, 0, 0, 0, 0),
(2, 'BIG NAME', 'TESTTESTTEST', 2, 1295896006, 1, 'BHUSHAN', 17, 1336226136, 1, 'BHUSHAN', 2, 142, 0, 1, 0, 0),
(3, 'dsgdfg', '', 2, 1336139587, 1, 'BHUSHAN', 15, 1336139587, 1, 'BHUSHAN', 0, 2, 0, 0, 0, 0),
(4, 'testtest', 'testtest', 2, 1336140071, 1, 'BHUSHAN', 16, 1336140071, 1, 'BHUSHAN', 0, 3, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wapbb_users`
--

CREATE TABLE IF NOT EXISTS `wapbb_users` (
  `user_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(32) NOT NULL DEFAULT '',
  `password_salt` varchar(8) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL,
  `prev_email` varchar(150) DEFAULT NULL,
  `hide_email` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `loaction` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `bday_day` int(2) DEFAULT NULL,
  `bday_month` int(2) DEFAULT NULL,
  `bday_year` int(4) DEFAULT NULL,
  `joined` int(10) NOT NULL DEFAULT '0',
  `user_verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_key` varchar(50) NOT NULL DEFAULT '0',
  `login_key` varchar(32) NOT NULL DEFAULT '',
  `login_key_expire` int(10) NOT NULL DEFAULT '0',
  `last_visit` int(10) NOT NULL DEFAULT '0',
  `last_post_time` int(10) NOT NULL DEFAULT '0',
  `num_posts` mediumint(6) NOT NULL DEFAULT '0',
  `num_topics` mediumint(6) NOT NULL DEFAULT '0',
  `sig` tinytext,
  `user_banned` tinyint(1) NOT NULL DEFAULT '0',
  `disable_pm` tinyint(1) NOT NULL DEFAULT '0',
  `hide_profile` tinyint(1) NOT NULL DEFAULT '0',
  `last_post_id` int(10) NOT NULL DEFAULT '0',
  `profile_views` int(10) NOT NULL DEFAULT '0',
  `site_url` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_mod` tinyint(1) NOT NULL DEFAULT '0',
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wapbb_users`
--

INSERT INTO `wapbb_users` (`user_id`, `username`, `password_hash`, `password_salt`, `email`, `prev_email`, `hide_email`, `ip_address`, `first_name`, `last_name`, `loaction`, `gender`, `bday_day`, `bday_month`, `bday_year`, `joined`, `user_verified`, `verification_key`, `login_key`, `login_key_expire`, `last_visit`, `last_post_time`, `num_posts`, `num_topics`, `sig`, `user_banned`, `disable_pm`, `hide_profile`, `last_post_id`, `profile_views`, `site_url`, `is_admin`, `is_mod`, `is_banned`) VALUES
(1, 'BHUSHAN', '2f735ca310be474f9fae65a93e1709b6', 'bM26C$mU', 'bhushan.online@yahoo.com', NULL, 0, '127.0.0.1', 'Bhushan', 'Patil', 'Navi Mumbai', 1, 3, 8, 1990, 1295784497, 0, '1808289', '', 0, 1336330236, 1336226136, 2, 0, NULL, 0, 0, 0, 0, 0, 'http://thinkwap.tk', 0, 0, 0),
(2, 'AKON', 'dde4eb60247815c4d11c00c44e9079c8', ';@+E_I8d', 'konvict@mail.com', NULL, 0, '127.0.0.1', '', '', '', 0, NULL, NULL, NULL, 1295784592, 0, '1014923', '', 0, 1336226542, 0, 0, 0, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
