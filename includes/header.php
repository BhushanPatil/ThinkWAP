<?php

if (!defined("WAPBB"))
{
    exit;
}

$time = time();
$ip = get_ip();
$ua = get_ua();
$php_self = $_SERVER["PHP_SELF"];

if (isset($_SERVER["HTTP_REFERER"]))
{
    $http_referer = $_SERVER["HTTP_REFERER"];
}
else
{
    $http_referer = "";
}

if (is_logged())
{
    $user = get_user($_SESSION["username"]);
}
elseif (isset($_COOKIE["log_user"]) && isset($_COOKIE["log_pass"]))
{
    $log_user = $_COOKIE["log_user"];
    $log_pass = $_COOKIE["log_pass"];
    
    $username = xoft_decode($log_user, $config_keypass);
    $password = xoft_decode($log_pass, $config_keypass);
    
    if (login($username, $password) == true)
    {
        $user = get_user($username);
        $_SESSION["username"] = $user["username"];
        mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET last_visit = '".$time."' WHERE username = '".$user["username"]."'");
    }
    else
    {
        $domain = get_host(SITE_URL);
        $secure = isset($_SERVER["HTTPS"]);
        setcookie("log_key", "", $time - 3600, "/", $domain, $secure, true);
        unset($_COOKIE["log_key"]);
        setcookie("log_user", "", $time - 3600, "/", $domain, $secure, true);
        unset($_COOKIE["log_user"]);
        setcookie("log_pass", "", $time - 3600, "/", $domain, $secure, true);
        unset($_COOKIE["log_pass"]);
    }
}

if (isset($_GET["action"]))
{
    $action = $_GET["action"];
}
else
{
    $action = "";
}

if (stristr($php_self, "/forum.php") || stristr($php_self, "/new_topic.php"))
{
    if (isset($_GET["fid"]) && !empty($_GET["fid"]))
    {
        $forum_id = abs(intval($_GET["fid"]));
        $forum = get_forum($forum_id);
        if ($forum == false)
        {
            header("Location: ".SITE_URL);
            exit;
        }
    }
    else
    {
        header("Location: ".SITE_URL);
        exit;
    }
}
elseif (stristr($php_self, "/topic.php") || stristr($php_self, "/reply.php"))
{
    if (isset($_GET["tid"]) && !empty($_GET["tid"]))
    {
        $topic_id = abs(intval($_GET["tid"]));
        $topic = get_topic($topic_id);
        if ($topic == false)
        {
            header("Location: ".SITE_URL);
            exit;
        }
        else
        {
            $forum = get_forum($topic["topic_forum_id"]);
        }
        if (isset($_GET["q"]))
        {
            $quote_post_id = abs(intval($_GET["q"]));
        }
    }
    else
    {
        header("Location: ".SITE_URL);
        exit;
    }
}
// Problem to fix: anyone can edit a post
elseif (stristr($php_self, "/editpost.php"))
{
    if (isset($_GET["pid"]))
    {
        $post_id = abs(intval($_GET["pid"]));
        $post = get_post($post_id);
        if ($post == false)
        {
            header("Location: ".SITE_URL);
            exit;
        }
        else
        {
            $topic = get_topic($post["post_topic_id"]);
            $forum = get_forum($topic["topic_forum_id"]);
        }
    }
    else
    {
        header("Location: ".SITE_URL);
        exit;
    }
}
elseif (stristr($php_self, "/user.php"))
{
    if (isset($_GET["uid"]))
    {
        $profile_id = abs(intval($_GET["uid"]));
        $profile = get_user_by_id($profile_id);
        if ($profile == false)
        {
            header("Location: ".SITE_URL);
            exit;
        }
    }
    else
    {
        header("Location: ".SITE_URL);
        exit;
    }
}
elseif (stristr($php_self, "/pm.php") && ($action == "read" || $action == "reply"))
{
    if (isset($_GET["pmid"]))
    {
        $pm_id = abs(intval($_GET["pmid"]));
        $message = read_pm($pm_id, $user["user_id"]);
        if ($message == false)
        {
            header("Location: ".SITE_URL);
            exit;
        }
    }
    else
    {
        header("Location: ".SITE_URL);
        exit;
    }
}

if (stristr($php_self, "/index.php"))
{
    $my_title = "";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage");
}
elseif (stristr($php_self, "/forum.php"))
{
    $my_title = $forum["forum_name"];
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("forum.php?fid=".$forum_id, htmlspecialchars($forum["forum_name"]), $forum["forum_name"]);
}
elseif (stristr($php_self, "/new_topic.php"))
{
    $my_title = "Posting A New Topic In A ".$forum["forum_name"];
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("forum.php?fid=".$forum_id, htmlspecialchars($forum["forum_name"]), $forum["forum_name"]);
}
elseif (stristr($php_self, "/topic.php"))
{
    $my_title = $topic["title"];
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("forum.php?fid=".$topic["topic_forum_id"], htmlspecialchars($forum["forum_name"]), $forum["forum_name"])."&gt;".anchor("topic.php?tid=".$topic_id, htmlspecialchars($topic["title"]), $topic["title"]);
}
elseif (stristr($php_self, "/reply.php"))
{
    $my_title = "Replying To ".$topic["title"];
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("forum.php?fid=".$topic["topic_forum_id"], htmlspecialchars($forum["forum_name"]), $forum["forum_name"])."&gt;".anchor("topic.php?tid=".$topic_id, htmlspecialchars($topic["title"]), $topic["title"]);
}
elseif (stristr($php_self, "/editpost.php"))
{
    $my_title = "Editing a post in ".$topic["title"];
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("forum.php?fid=".$topic["topic_forum_id"], htmlspecialchars($forum["forum_name"]), $forum["forum_name"])."&gt;".anchor("topic.php?tid=".$post["post_topic_id"], htmlspecialchars($topic["title"]), $topic["title"]);
}
elseif (stristr($php_self, "/search.php"))
{
    $my_title = "Search";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("search.php", "Search", "Search ".SITE_TITLE);
}
elseif (stristr($php_self, "/login.php"))
{
    $my_title = "Login";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("login.php", "Login", "Login");
}
elseif (stristr($php_self, "/logout.php"))
{
    $my_title = "Logout";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("logout.php", "Logout", "Logout");
}
elseif (stristr($php_self, "/register.php"))
{
    $my_title = "Registration";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("register.php", "Registration", "Registration");
}
elseif (stristr($php_self, "/pm.php"))
{
    if ($action == "compose")
    {
        $my_title = "Compose a Private Message";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("pm.php", "Messages", "Private Messages")."&#187;".anchor("pm.php?action=compose", "Compose", "Compose a Private Message");
    }
    if ($action == "read")
    {
        $my_title = "Viewing PM: ".$message["subject"];
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("pm.php", "Messages", "Private Messages")."&#187;".anchor("pm.php?action=read", $message["subject"], "Viewing PM: ".$message["subject"]);
    }
    else
    {
        $my_title = "Messages";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("pm.php", "Messages", "Messages");
    }
}
elseif (stristr($php_self, "/user.php"))
{
    $my_title = $user["username"]."'s Profile";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("user.php", $user["username"]."'s Profile", $user["username"]."'s Profile");
}
elseif (stristr($php_self, "/usercp.php"))
{
    if ($action == "password")
    {
        $my_title = "Change Password";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("usercp.php", "User Control Panel", "User Control Panel");
    }
    elseif ($action == "email")
    {
        $my_title = "Change Email Address";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("usercp.php", "User Control Panel", "User Control Panel");
    }
    elseif ($action == "profile")
    {
        $my_title = "Edit Profile Information";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("usercp.php", "User Control Panel", "User Control Panel");
    }
    else
    {
        $my_title = "Settings";
        $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("usercp.php", "User Control Panel", "User Control Panel");
    }
}
elseif (stristr($php_self, "/lost_password.php"))
{
    $my_title = "Lost Password";
    $navbar = anchor(SITE_URL, image("images/home.gif", "Home", 10, 10)." ".SITE_TITLE, SITE_TITLE." Homepage")."&#187;".anchor("lost_password.php", "Lost Password", "Lost Password");
}
else
{
    $my_title = "";
    $navbar = "";
}

if ($config_no_cache == 0)
{
	header("Cache-Control: no-cache, must-revalidate");
    header("Expires: ".date("r"));
}
else
{
    header("Cache-Control: public");
	header("Expires: ".date("r", time() + 600));
}

if (is_ip_banned($ip))
{
    require_once "includes/start.php";
    require_once "includes/functions.php";
    require_once "includes/header.php";
    include_once "themes/".$config_theme."/index.php";
    echo "<div class=\"content\">\n";
    echo "<div class=\"message error\">\n";
    echo "<b>Your account is currently banned.</b><br />\n";
    echo "Ban Reason: Unknown<br />\n";
    echo "Ban will be lifted: ".time()."<br />\n";
    echo "</div>\n";
    echo "</div>\n";
    include_once "themes/".$config_theme."/foot.php";
}
elseif (is_logged() && stristr($php_self, "/logout.php") == false)
{
    if ($user["is_banned"] == 1)
    {
        require_once "includes/start.php";
        require_once "includes/functions.php";
        require_once "includes/header.php";
        include_once "themes/".$config_theme."/index.php";
        echo "<div class=\"content\">\n";
        echo "<div class=\"message error\">\n";
        echo "<b>Your account is currently banned.</b><br />\n";
        echo "Ban Reason: Unknown<br />\n";
        echo "Ban will be lifted: ".time()."<br />\n";
        echo "</div>\n";
        echo "</div>\n";
        include_once "themes/".$config_theme."/foot.php";
    }
}

?>