<?php

//@error_reporting(E_ALL);
//@error_reporting(E_ALL & ~E_NOTICE);
//@error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
ini_set("display_errors", true);
ini_set("html_errors", false);
ini_set("error_reporting", E_ALL);
define("WAPBB", "0.0.4");

list($generic_msec, $generic_sec) = explode(chr(32), microtime());
$generic_time = $generic_sec + $generic_msec;
unset($generic_sec, $generic_msec);

if (version_compare(PHP_VERSION, "5.2.1") <= 0)
{
	echo "You must be using PHP 5.2.1 or better. You are currently using: ".PHP_VERSION;
    exit;
}

session_name("SID");
session_start();


include_once "includes/connect.php";

if (!defined("SQL_HOST") || !defined("SQL_DATABASE"))
{
    header("Location: install.php" );
}


mysql_connect(SQL_HOST, SQL_USERNAME, SQL_PASSWORD)or die ("Could not connect to server: ".mysql_error());
mysql_select_db(SQL_DATABASE)or die("Could not select database: ".mysql_error());

if (get_magic_quotes_gpc())
{
    $_GET = array_map("stripslashes", $_GET);
    $_POST = array_map("stripslashes", $_POST);
    $_COOKIE = array_map("stripslashes", $_COOKIE);
}

$_GET = array_map("trim", $_GET);
$_POST = array_map("trim", $_POST);
$_COOKIE = array_map("trim", $_COOKIE);
if (function_exists("mysql_real_escape_string"))
{
    $_GET = array_map("mysql_real_escape_string", $_GET);
    $_POST = array_map("mysql_real_escape_string", $_POST);
    $_COOKIE = array_map("mysql_real_escape_string", $_COOKIE);
}
else
{
    function escape_string($string)
    {
        $search = array("\\","\0","\n","\r","\x1a","'",'"');
        $replace = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
        return str_replace($search, $replace, $string);
    }
    $_GET = array_map("escape_string", $_GET);
    $_POST = array_map("escape_string", $_POST);
    $_COOKIE = array_map("escape_string", $_COOKIE);
}

/*
foreach ($_GET as $check_url)
{
    if (is_string($check_url) && !preg_match('#^(?:[a-z0-9_\-/]+|\.+(?!/))*$#i', $check_url))
    {
        echo $_SERVER["QUERY_STRING"];
        header("Location: index.php");
		exit;
	}
}
unset($check_url);
//*/

$config_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."config_settings");
$config = mysql_fetch_array($config_query);

define("SITE_URL", "http://localhost/thinkwap.tk");
define("SITE_TITLE", "wapBB");


$config_keywords = "";
$config_description = "";


$config_image_logo = SITE_URL."/images/logo.png";
$config_theme = "deafult";
$config_favicon = SITE_URL."/favicon.ico";
$config_slogan = "Free Wap Scripts";
$config_copy = "&copy; 2010-".date("y")." BHUSHAN";
$config_keypass = "B_D_P_K_M_K_L";

$config_time_clocks = 5;
$config_no_cache = 1;

$config_admin_email = "bhushan.online@yahoo.com";
$config_accoutn_activation = 1;
$config_admin_notify = 1;

$config_attachment = 1;
$config_attach_max_size = 2097152; //The maximum size in bytes of the attachment file. 

$config_emoticons = 1;
$config_emoticons_set = "default";
$config_forum_navbar = 1;
$config_topics_per_page = 10;
$config_posts_per_page = 5;
$config_messages_per_page = 10;
$config_message_replies_per_page = 1;

$config_captcha = 1;
$config_captcha_word_length = 4;
$config_captcha_chars = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9");
$config_captcha_pass = "8iGP4P4#lOl";
$config_captcha_bg = "";
$config_captcha_font = "";
$config_captcha_font_size = "";

date_default_timezone_set("Asia/Calcutta");

//define("SITETIME", time() + $config_time_clocks * 3600);

?>