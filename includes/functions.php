<?php

if (!defined("WAPBB"))
{
    exit;
}

//
function sql_connect($sql_host, $sql_user, $sql_pass, $sql_db_name)
{
    $sql_connect = mysql_connect($sql_host, $sql_user, $sql_pass);
    if (!$sql_connect)
    {
        return false;
    }
    $sql_select_db = mysql_select_db($sql_db_name);
    if (!$sql_select_db)
    {
        return false;
    }
    return true;
}

//
function reverse_mysql_real_escape_string($str)
{
    $search = array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"');
    $replace = array("\\", "\0", "\n", "\r", "\x1a", "'", '"');
    return str_replace($search, $replace, $str);
}

//
function get_ip()
{
    if (isset($_SERVER))
	{
		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		{
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			//$proxy = $_SERVER["REMOTE_ADDR"];
		}
        elseif (isset($_SERVER["HTTP_CLIENT_IP"]))
		{
			$ip = $_SERVER["HTTP_CLIENT_IP"];
		}
        elseif (isset($_SERVER["HTTP_X_REAL_IP"]))
		{
			$ip = $_SERVER["HTTP_X_REAL_IP"];
		}
		else
		{
			$ip = $_SERVER["REMOTE_ADDR"];
		}
	}
	else
	{
		if (getenv("HTTP_X_FORWARDED_FOR"))
		{
			$ip = getenv("HTTP_X_FORWARDED_FOR");
			//$proxy = getenv('REMOTE_ADDR');
		}
        elseif (getenv("HTTP_CLIENT_IP"))
		{
			$ip = getenv("HTTP_CLIENT_IP");
		}
		else
		{
			$ip = getenv("REMOTE_ADDR");
		}
	}
    return $ip;
}

//
function get_ua()
{
    $keyname_ua_arr = array("HTTP_X_DEVICE_USER_AGENT", "HTTP_X_OPERAMINI_PHONE_UA", "HTTP_USER_AGENT");
	foreach ($keyname_ua_arr as $keyname_ua)
	{
		if (!empty($_SERVER[$keyname_ua]))
		{
			$ua = $_SERVER[$keyname_ua];
			break;
		}
	}
    return $ua;
}

//
function get_host($url)
{
    $parse_url = parse_url(trim($url));
    return trim($parse_url["host"] ? $parse_url["host"] : array_shift(explode("/", $parse_url["path"], 2)));
}

//
function get_domain()
{
    return SITE_URL;
}

function parse_extras($rule)  
{
    if ($rule[0] == "#")
    {
        $id = substr($rule, 1, strlen($rule));
        $data = " id=\"".$id."\"";
        return $data;
    }
    if ($rule[0] == ".")
    {
        $class = substr($rule, 1, strlen($rule));
        $data = " class=\"".$class."\"";
        return $data;
    }
    if ($rule[0] == "_")
    {
        $data = " target=\"".$rule."\"";
        return $data;
    }
    if ($rule[0] == "&")
    {
        $rel = substr($rule, 1, strlen($rule));
        $data = " rel=\"".$rel."\"";
        return $data;
    }
}

//
function anchor($link, $text = "", $title = "", $extras = "")
{
    if (strpos($link, "http://") || strpos($link, "https://") || strpos($link, "ftp://"))
    {
        $link = get_domain()."/".$link;
    }
    $data = "<a href=\"".$link."\"";
    if ($title != false)
    {
        if ($title != "")
        {
            $title = htmlspecialchars($title);
            $data .= " title=\"".$title."\"";
        }
        else
        {
            $title = htmlspecialchars($text);
            $data .= " title=\"".$title."\"";
        }
    }
    if ($extras != false || $extras != "")
    {
        if (is_array($extras))
        {
            foreach($extras as $rule)
            {
                $data .= parse_extras($rule);
            }
        }
        if (is_string($extras))
        {
            $data .= parse_extras($extras);
        }
    }
    $data.= ">";
    if ($text == false || $text == "")
    {
        $data .= $link;
    }
    else
    {
        //if (preg_match("</?[a-z][a-z0-9]*[^<>]*>", $text))
        //{
            $data .= $text;
        //}
        //else
        //{
            //$data .= htmlspecialchars($text);
        //}
    }
    $data .= "</a>";
    return $data;  
}

function image($file, $alt = "", $width = "", $height = "", $extras = "")
{
    $data = "<img src=\"".$file."\"";
    if ($alt == "")
    {
        if (strpos($file, "http://"))
        {
            $alt = str_replace("http://", "", $file);
            $alt = explode("/", $alt);
            $alt = $alt[0];
        }
        else
        {
            $alt = explode("/", $file);
            $alt = $alt[count($alt) - 1];
        }
        $alt = explode(".", $alt);
        $alt = htmlspecialchars($alt[0]);
        $data .= " alt=\"".$alt."\"";
    }
    else
    {
        $data .= " alt=\"".$alt."\"";
    }
    if ($width == "" || $height == "")
    {
        if (list($width, $height) = getimagesize($file))
        {
            $data .= " width=\"".$width."\" height=\"".$height."\"";
        }
    }
    else
    {
        $data .= " width=\"".$width."\" height=\"".$height."\"";
    }
    if ($extras != false || $extras != "")
    {
        if (is_array($extras))
        {
            foreach($extras as $rule)
            {
                $data .= parse_extras($rule);
            }
        }
        if (is_string($extras))
        {
            $data .= parse_extras($extras);
        }
    }
    $data .= " />";
    return $data;
}


//
function login($username, $password)
{
    $user_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE username = '".$username."' LIMIT 1");
    if (mysql_num_rows($user_query) == 1)
    {
        $user = mysql_fetch_array($user_query);
        $password_salt = $user["password_salt"];
        $password_hash = password_hash($password_salt, $password);
        if ($password_hash == $user["password_hash"])
        {
            return true;
        }
    }
    return false;
}

//
function is_logged()
{
    if (isset($_SESSION["username"]) && $_SESSION["username"] != "")
    {
        if (get_user($_SESSION["username"]) == false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

//
function get_user($username)
{
    $user_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE username = '".$username."'");
    if (mysql_numrows($user_query) == 1)
    {
        $user_array = mysql_fetch_array($user_query);
        return $user_array;
    }
    return false;
}

//
function get_user_by_id($user_id)
{
    $user_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users WHERE user_id = '".$user_id."'");
    if (mysql_numrows($user_query) == 1)
    {
        $user_array = mysql_fetch_array($user_query);
        return $user_array;
    }
    return false;
}

//
function get_forum($forum_id)
{
    $forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE forum_id = '".$forum_id."'");
    if (mysql_numrows($forum_query) == 1)
    {
        $forum_array = mysql_fetch_array($forum_query);
        return $forum_array;
    }
    return false;
}

//
function get_topic($topic_id)
{
    $post_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics WHERE topic_id = '".$topic_id."'");
    if (mysql_numrows($post_query) == 1)
    {
        $post_array = mysql_fetch_array($post_query);
        return $post_array;
    }
    return false;
}

//
function get_post($post_id)
{
    $post_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."posts WHERE post_id = '".$post_id."'");
    if (mysql_numrows($post_query) == 1)
    {
        $post_array = mysql_fetch_array($post_query);
        return $post_array;
    }
    return false;
}

//
function read_pm($pm_id, $user_id)
{
    $message_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."private_messages WHERE pm_id = '".$pm_id."'");
    if (mysql_numrows($message_query) == 1)
    {
        $message = mysql_fetch_array($message_query);
        if ($message["from_id"] == $user_id || $message["to_id"] == $user_id)
        {
            return $message;
        }
    }
    return false;
}

//
// Finds group type and returns group type and false if no such group type.
//
// @param integer $typeid Group type id
// @return bool
function group_by_id($typeid)
{
	if ($typeid == 0)
	{
		return "Guest"; // Guest/Not registered user
	}
    elseif ($typeid == 1)
	{
		return "Registered"; // Simple registred user
	}
    elseif ($typeid == 2)
	{
		return "Moderator"; // Who can access mod panel and has rights of mod
	}
    elseif ($typeid == 3)
	{
		return "Administrator"; // Who can access admin panel has rights of admin
	}
	return false;
}

function count_members()
{
    $count_members_query = mysql_query("SELECT COUNT(user_id) FROM ".SQL_TABLE_PERFIX."users");
    $count_members = mysql_fetch_array($count_members_query);
    return $count_members[0];
}

function last_member()
{
    $last_member_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."users ORDER BY user_id DESC LIMIT 1");
    $last_member = mysql_fetch_array($last_member_query);
    return $last_member;
}

//
function get_age($BirthYear, $BirthMonth, $BirthDay)
{
        // Find the differences
        $YearDiff = date("Y") - $BirthYear;
        $MonthDiff = date("m") - $BirthMonth;
        $DayDiff = date("d") - $BirthDay;
        
        // If the birthday has not occured this year
        if ($DayDiff < 0 || $MonthDiff < 0)
        {
            $YearDiff--;
        }
        return $YearDiff;
}

// Create a password hash 
function password_hash($salt, $password)
{
    return md5(md5($salt).md5($password));
}

// Creates a character sequence for password
function passsword_salt($length = 8)
{
    $salt = "";
    
    //$string = md5(uniqid(rand(), true));
    //$salt = substr($string, 0, $length);
    
    for ($i = 0; $i < $length; $i++)
    {
        $num = mt_rand(33, 126);
        if ($num == "92")
        {
            $num = 93;
        }
        $salt .= chr($num);
    }
    return $salt;
}

// Create a random 15 character password
function make_password($length = 15)
{
	$unique_id_1 = uniqid(mt_rand(), true);
	$unique_id_2 = uniqid(mt_rand(), true);
	$final_rand = md5($unique_id_1.$unique_id_2);
	for ($i = 0; $i < $length; $i++)
	{
		$pass = $final_rand{mt_rand(0, 31)};
	}
	return $pass;
}

// Clean a string to remove all non alphanumeric characters
function alphanumerical_clean($text, $additional_tags = "")
{
    if ($additional_tags)
    {
        $additional_tags = preg_quote($additional_tags, "/");
    }
    return preg_replace( "/[^a-zA-Z0-9\-\_".$additional_tags."]/", "" , $text);
}

//
function url_replace($m)
{
    if (!isset($m[2]))
    {
        $target = (strpos($m[1], SITE_URL) == false) ? "_blank" : "";
        //return "<a href=\"".$m[1]."\"".$target.">".$m[1]."</a>";
        return anchor($m[1], $m[1], $m[1], $target);
    }
    elseif (!isset($m[3]))
    {
        $target = (strpos($m[1], SITE_URL) == false) ? "_blank" : "";
        //return "<a href=\"".$m[1]."\"".$target.">".$m[2]."</a>";
        return anchor($m[1], $m[2], $m[2], $target);
    }
    else
    {
        $target = (strpos($m[3], SITE_URL) == false) ? "_blank" : "";
        //return "<a href=\"".$m[3]."\"".$target.">".$m[3]."</a>";
        return anchor($m[3], $m[3], $m[3], $target);
    }
}

//
function highlight_code($code)
{
    //$code = emoticons($code);
    $code = strtr($code, array('&lt;' => '<', '&gt;' => '>', '&amp;' => '&', '&quot;' => '"', '&#36;' => '$', '&#37;' => '%', '&#39;' => "'", '&#92;' => '\\', '&#94;' => '^', '&#96;' => '`', '&#124;' => '|', '<br />' => "\r\n"));
    $code = highlight_string($code, true);
    $code = strtr($code, array("\r\n" => '<br />', '$' => '&#36;', "'" => '&#39;', '%' => '&#37;', '\\' => '&#92;', '`' => '&#96;', '^' => '&#94;', '|' => '&#124;'));
    $code = '<div class="code">'.$code.'</div>';
    return $code;
}

/*
function bb_code($text)
{
    $text = htmlentities($text);
    $text = nl2br($text);
    $text = str_replace("  ", "&nbsp;&nbsp;", $text);
    $text = preg_replace('#\[code\](.*?)\[/code\]#si', '<code>\1</code>', $text);
    $text = preg_replace('#\[hide\](.*?)\[/hide\]#ie', 'hidden_text("\1")', $text);
    $text = preg_replace('#\[big\](.*?)\[/big\]#si', '<big>\1</big>', $text);
    $text = preg_replace('#\[b\](.*?)\[/b\]#si', '<b>\1</b>', $text);
    $text = preg_replace('#\[i\](.*?)\[/i\]#si', '<i>\1</i>', $text);
    $text = preg_replace('#\[u\](.*?)\[/u\]#si', '<u>\1</u>', $text);
    $text = preg_replace('#\[small\](.*?)\[/small\]#si', '<small>\1</small>', $text);
    $text = preg_replace('#\[red\](.*?)\[/red\]#si', '<span style="color:#ff0000">\1</span>', $text);
    $text = preg_replace('#\[green\](.*?)\[/green\]#si', '<span style="color:#00cc00">\1</span>', $text);
    $text = preg_replace('#\[blue\](.*?)\[/blue\]#si', '<span style="color:#0000ff">\1</span>', $text);
    $text = preg_replace('#\[q\](.*?)\[/q\]#si', '<div class="q">\1</div>', $text);
    $text = preg_replace('#\[del\](.*?)\[/del\]#si', '<del>\1</del>', $text);
    $text = preg_replace_callback('~\\[url=(http://.+?)\\](.+?)\\[/url\\]|(http://(www.)?[0-9a-z\.\-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\-\~&;_=%:#\+]*)~si', 'url_replace', $text);
    $text = preg_replace_callback('~\\[url=(https://.+?)\\](.+?)\\[/url\\]|(https://(www.)?[0-9a-z\.\-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\-\~&;_=%:#\+]*)~si', 'url_replace', $text);
    $text = preg_replace_callback('~\\[url=(ftp://.+?)\\](.+?)\\[/url\\]|(ftp://(www.)?[0-9a-z\.\-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\-\~&;_=%:#\+]*)~si', 'url_replace', $text);
    
    $text = preg_replace("(\[mail\](.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,4})(\]?))\[/mail\])", "<a href=\"mailto:$1\" target=\"_blank\">$1</a>", $text);
    $text = preg_replace("(\[mail\=(.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,4})(\]?))\](.+?)\[/mail\])", "<a href=\"mailto:$1\" target=\"_blank\">$5</a>", $text);
    
    $text = preg_replace("#\[list\](.+?)\[\/list\]#is", "<ul class=\"bullet\">$1</ul>", $text);
	$text = preg_replace("#\[list=1\](.+?)\[\/list\]#is", "<ul class=\"decimal\">$1</ul>", $text);
	$text = preg_replace("#\[list=i\](.+?)\[\/list\]#s", "<ul class=\"lowerroman\">$1</ul>", $text);
	$text = preg_replace("#\[list=I\](.+?)\[\/list\]#s", "<ul class=\"upperroman\">$1</ul>", $text);
	$text = preg_replace("#\[list=a\](.+?)\[\/list\]#s", "<ul class=\"loweralpha\">$1</ul>", $text);
	$text = preg_replace("#\[list=A\](.+?)\[\/list\]#s", "<ul class=\"upperalpha\">$1</ul>", $text);
	$text = str_replace("[*]", "<li>", $text);
    
    $text = preg_replace("#(\[quote\](.+?)\[\/quote])#is", "<div style=\"padding:0px 0px 0px 5px;\"><small>Quote:</small></div>\n<div style=\"border: 1px solid silver;padding:10px;background-color:#f9f9f9;\">$1</div>\n", $text);
    
    while (strpos($text, "[quote=") !== false)
	{
		list($text1, $text2) = explode("[quote=", $text, 2);
		if (strpos($text2, "]") !== false)
		{
			list($text2, $text3) = explode("]", $text2, 2);
			if (strpos($text3, "[/quote]") !== false)
			{
				list($text3, $text4) = explode("[/quote]", $text3, 2);
                $post = get_post($text2);
				$text = $text1."<div style=\"padding:0px 0px 0px 5px;\"><small>".$post["poster_name"].", on ".date("j M Y - h:i A", $post["post_time"]).", said:</small></div>\n<div style=\"border: 1px solid silver;padding:10px;background-color:#f9f9f9;\">".$text3."</div>\n".$text4;
			}
			else
			{
				$text = $text1."[quote\n=".$text2."]".$text3;
			}
		}
		else
		{
			$text = $text1."[quote\n=".$text2;
		}
	}
    return $text;
}

//
function bbcode($Text)
{
    // Replace any html brackets with HTML Entities to prevent executing HTML or script
    // Don't use strip_tags here because it breaks [url] search by replacing & with amp
    
    // Convert new line chars to html <br /> tags
    $Text = nl2br($Text);
    
    // Set up the parameters for a URL search string
    $URLSearchString = " a-zA-Z0-9\:\&\/\-\?\.\=\_\~\#\'";
    
    // Set up the parameters for a MAIL search string
    $MAILSearchString = $URLSearchString." a-zA-Z0-9\.@";
    
    // Perform URL Search
    $Text = preg_replace("(\[url\]([$URLSearchString]*)\[/url\])is", "<a href=\"$1\" target=\"_blank\">$1</a>", $Text);
    $Text = preg_replace("(\[url\=([$URLSearchString]*)\]([$URLSearchString]*)\[/url\])is", "<a href=\"$1\" target=\"_blank\">$2</a>", $Text);
    
    //$Text = preg_replace_callback("~\\[url=(http://.+?)\\](.+?)\\[/url\\]|(http://(www.)?[0-9a-z\.\-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\-\~&;_=%:#\+]*)~", "url_replace", $Text);
    
	// Perform MAIL Search
	$Text = preg_replace("(\[mail\]([$MAILSearchString]*)\[/mail\])", "<a href=\"mailto:$1\">$1</a>", $Text);
	$Text = preg_replace("/\[mail\=([$MAILSearchString]*)\](.+?)\[\/mail\]/", "<a href=\"mailto:$1\">$2</a>", $Text);

	// Check for bold text
	$Text = preg_replace("(\[b\](.+?)\[\/b])is", "<b>$1</b>", $Text);

	// Check for Italics text
	$Text = preg_replace("(\[i\](.+?)\[\/i\])is", "<I>$1</I>", $Text);

	// Check for Underline text
	$Text = preg_replace("(\[u\](.+?)\[\/u\])is", "<u>$1</u>", $Text);

	// Check for strike-through text
	$Text = preg_replace("(\[s\](.+?)\[\/s\])is", "<span class=\"strikethrough\">$1</span>", $Text);

	// Check for over-line text
	$Text = preg_replace("(\[o\](.+?)\[\/o\])is", "<span class=\"overline\">$1</span>", $Text);

	// Check for colored text
	$Text = preg_replace("(\[color=(.+?)\](.+?)\[\/color\])is", "<span style=\"color: $1\">$2</span>", $Text);

	// Check for sized text
	$Text = preg_replace("(\[size=(.+?)\](.+?)\[\/size\])is", "<span style=\"font-size: $1px\">$2</span>", $Text);

	// Check for list text
	$Text = preg_replace("/\[list\](.+?)\[\/list\]/is", "<ul class=\"listbullet\">$1</ul>", $Text);
	$Text = preg_replace("/\[list=1\](.+?)\[\/list\]/is", "<ul class=\"listdecimal\">$1</ul>", $Text);
	$Text = preg_replace("/\[list=i\](.+?)\[\/list\]/s", "<ul class=\"listlowerroman\">$1</ul>", $Text);
	$Text = preg_replace("/\[list=I\](.+?)\[\/list\]/s", "<ul class=\"listupperroman\">$1</ul>", $Text);
	$Text = preg_replace("/\[list=a\](.+?)\[\/list\]/s", "<ul class=\"listloweralpha\">$1</ul>", $Text);
	$Text = preg_replace("/\[list=A\](.+?)\[\/list\]/s", "<ul class=\"listupperalpha\">$1</ul>", $Text);
	$Text = str_replace("[*]", "<li>", $Text);
    
    // Check for quote
	$Text = preg_replace("(\[quote\](.+?)\[\/quote])is", "<div style=\"border: 1px solid #000000;\">\n<div style=\"border-bottom: 1px solid #000000;padding:5px;\">Quote:</div>\n<div style=\"padding:10px;\">$1</div>\n</div>", $Text);
    while (strpos($Text, "[quote=") !== false)
	{
		list($Text1, $Text2) = explode("[quote=", $Text, 2);
		if (strpos($Text2, "]") !== false)
		{
			list($Text2, $Text3) = explode("]", $Text2, 2);
			if (strpos($Text3, "[/quote]") !== false)
			{
				list($Text3, $Text4) = explode("[/quote]", $Text3, 2);
                $post = get_post($Text2);
				$Text = $Text1."<div style=\"padding:0px 0px 0px 5px;\"><small>".$post["poster_name"].", on ".date("j M Y - h:i A", $post["post_time"]).", said:</small></div>\n<div style=\"border: 1px solid silver;padding:10px;background-color:#f9f9f9;\">".$Text3."</div>\n".$Text4;
			}
			else
			{
				$Text = $Text1."[quote\n=".$Text2."]".$Text3;
			}
		}
		else
		{
			$Text = $Text1."[quote\n=".$Text2;
		}
	}
	//$Text = str_replace("[quote\n=", "[quote=", $Text);
    
    
    // Check for code
	$Text = preg_replace("(\[code\](.+?)\[\/code])is", "<table class=\"quotecode\"><tr><td>Code:<br>$1</td></tr></table>", $Text);
    
	// Check for font change text
	$Text = preg_replace("(\[font=(.+?)\](.+?)\[\/font\])", "<span style=\"font-family: $1;\">$2</span>", $Text);
    
    // Images
	// [img]pathtoimage[/img]
	$Text = preg_replace("/\[img\](.+?)\[\/img\]/is", "<img src=\"$1\">", $Text);
	// [img=widthxheight]image source[/img]
	$Text = preg_replace("/\[img\=([0-9]*)x([0-9]*)\](.+?)\[\/img\]/", "<img src=\"$3\" height=\"$2\" width=\"$1\">", $Text);
	
    return $Text;
}
//*/

function bbcode($text, $emoticons = true)
{
    $text = htmlspecialchars($text);
    $text = preg_replace("#\[b\](.*?)\[/b\]#si", "<span style=\"font-weight: bold;\">$1</span>", $text);
    $text = preg_replace("#\[u\](.*?)\[/u\]#si", "<span style=\"text-decoration: underline;\">$1</span>", $text);
    $text = preg_replace("#\[i\](.*?)\[/i\]#si", "<span style=\"font-style: italic;\">$1</span>", $text);
    $text = preg_replace("#\[s\](.*?)\[/s\]#si", "<del>$1</del>", $text);
    $text = preg_replace("#\(c\)#i", "&copy;", $text);
    $text = preg_replace("#\(tm\)#i", "&#153;", $text);
    $text = preg_replace("#\(r\)#i", "&reg;", $text);
    
    /*
    $text = preg_replace("#\[code\](.*?)\[/code\](\r\n?|\n?)#ise", "bbcode_parse_code('$1', false, true)", $text);
    $text = preg_replace("#\[code=php\](.*?)\[/code\](\r\n?|\n?)#ise", "bbcode_parse_php('$1', false, true)", $text);
    $text = preg_replace("#\[php\](.*?)\[/php\](\r\n?|\n?)#ise", "bbcode_parse_php('$1', false, true)", $text);
    */
    
    $text = preg_replace("#([\>\s\(\)])(http|https|ftp|news){1}://([^\/\"\s\<\[\.]+\.([^\/\"\s\<\[\.]+\.)*[\w]+(:[0-9]+)?(/[^\"\s<\[]*)?)#i", "$1[url]$2://$3[/url]", $text);
    $text = preg_replace("#([\>\s\(\)])(www|ftp)\.(([^\/\"\s\<\[\.]+\.)*[\w]+(:[0-9]+)?(/[^\"\s<\[]*)?)#i", "$1[url]$2.$3[/url]", $text);
    $text = preg_replace("#\[url\]([a-z]+?://)([^\r\n\"<]+?)\[/url\]#sei", "bbcode_parse_url(\"$1$2\")", $text);
    $text = preg_replace("#\[url\]([^\r\n\"<]+?)\[/url\]#ei", "bbcode_parse_url(\"$1\")", $text);
    $text = preg_replace("#\[url=([a-z]+?://)([^\r\n\"<]+?)\](.+?)\[/url\]#esi", "bbcode_parse_url(\"$1$2\", \"$3\")", $text);
    $text = preg_replace("#\[url=([^\r\n\"<&\(\)]+?)\](.+?)\[/url\]#esi", "bbcode_parse_url(\"$1\", \"$2\")", $text);
    
    // Email
    $text = preg_replace("#\[email\](.*?)\[/email\]#ei", "bbcode_parse_email(\"$1\")", $text);
    $text = preg_replace("#\[email=(.*?)\](.*?)\[/email\]#ei", "bbcode_parse_email(\"$1\", \"$2\")", $text);
    $text = preg_replace("#\[hr\]#si", "<hr />", $text);
    $text = preg_replace("#\[color=([a-zA-Z]*|\#?[0-9a-fA-F]{6})](.*?)\[/color\]#si", "<span style=\"color: $1;\">$2</span>", $text);
    $text = preg_replace("#\[size=(xx-small|x-small|small|medium|large|x-large|xx-large)\](.*?)\[/size\]#si", "<span style=\"font-size: $1;\">$2</span>", $text);
    $text = preg_replace("#\[size=([0-9\+\-]+?)\](.*?)\[/size\]#esi", "bbcode_handle_size(\"$1\", \"$2\")", $text);
    $text = preg_replace("#\[font=([a-z ]+?)\](.+?)\[/font\]#si", "<span style=\"font-family: $1;\">$2</span>", $text);$text = preg_replace("#\[align=(left|center|right|justify)\](.*?)\[/align\]#si", "<div style=\"text-align: $1;\">$2</div>", $text);
    $text = preg_replace("#\[img\](.+?)\[\/img\]#ise", "image(\"$1\")", $text);
    $text = preg_replace("#\[img\=([0-9]*)x([0-9]*)\](.+?)\[\/img\]#ise", "image(\"$3\", null, $2, $1)", $text);
    $text = bbcode_parse_quotes($text);
    if ($emoticons == true)
    {
        $text = emoticons($text);
    }
    $text = nl2br($text);
    return $text;
}

function bbcode_parse_url($url, $name="")
{
    if(!preg_match("#^[a-z0-9]+://#i", $url))
    {
        $url = "http://".$url;
    }
    $fullurl = $url;
    
    $url = str_replace('&amp;', '&', $url);
    $name = str_replace('&amp;', '&', $name);
    
    if(!$name)
    {
        $name = $url;
    }
    
    $name = str_replace("\'", "'", $name);
    $url = str_replace("\'", "'", $url);
    $fullurl = str_replace("\'", "'", $fullurl);
    
    if ($name == $url)
    {
        if(strlen($url) > 55)
        {
            $name = substr($url, 0, 40)."...".substr($url, -10);
        }
    }
    
    // Fix some entities in URLs
    $entities = array(
        '$' => '%24',
        '&#36;' => '%24',
        '^' => '%5E',
        '`' => '%60',
        '[' => '%5B',
        ']' => '%5D',
        '{' => '%7B',
        '}' => '%7D',
        '"' => '%22',
        '<' => '%3C',
        '>' => '%3E',
        ' ' => '%20');
    
    $fullurl = str_replace(array_keys($entities), array_values($entities), $fullurl);
    
    $name = preg_replace("#&amp;\#([0-9]+);#si", "&#$1;", $name); // Fix & but allow unicode
    $link = "<a href=\"$fullurl\" target=\"_blank\">$name</a>";
    return $link;
}

function bbcode_parse_email($email, $name="")
{
    $name = str_replace("\\'", "'", $name);
    $email = str_replace("\\'", "'", $email);
    if(!$name)
    {
        $name = $email;
    }
    if(preg_match("/^([a-zA-Z0-9-_\+\.]+?)@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.-]+$/si", $email))
    {
        return "<a href=\"mailto:$email\">".$name."</a>";
    }
    else
    {
        return $email;
    }
}

function bbcode_handle_size($size, $text)
{
    $size = intval($size) + 10;
    if($size > 20)
    {
        $size = 20;
    }
    $text = "<span style=\"font-size: {$size}pt;\">".str_replace("\'", "'", $text)."</span>";
    return $text;
}

function bbcode_parse_quotes($message)
{
    $message = preg_replace("#\[quote=(?:&quot;|\"|')?(.*?)[\"']?(?:&quot;|\"|')?\](.*?)\[\/quote\](\r\n?|\n?)#esi", "bbcode_parse_post_quotes('$2','$1')", $message);
    $message = preg_replace("#\[quote\](.*?)\[\/quote\](\r\n?|\n?)#si", "<blockquote><cite>Quote:</cite>$1</blockquote>", $message);
    $message = preg_replace("#(\r\n*|\n*)<\/cite>(\r\n*|\n*)#", "</cite>", $message);
    $message = preg_replace("#(\r\n*|\n*)<\/blockquote>#", "</blockquote>", $message);
    return $message;
}

function bbcode_parse_post_quotes($message, $username)
{
    $linkback = $date = "";
    
    $message = trim($message);
    $message = preg_replace("#(^<br(\s?)(\/?)>|<br(\s?)(\/?)>$)#i", "", $message);
    
    if (!$message) return '';
    
    $message = str_replace('\"', '"', $message);
    $username = str_replace('\"', '"', $username)."'";
    $delete_quote = true;
    
    preg_match("#pid=(?:&quot;|\"|')?([0-9]+)[\"']?(?:&quot;|\"|')?#i", $username, $match);
    
    if (intval($match[1]))
    {
        $pid = intval($match[1]);
        $url = SITE_URL."/topic.php?tid=1#pid".$pid;
        //$linkback = " <a href=\"".$url."\">[ -> ]</a>";
        $username = preg_replace("#(?:&quot;|\"|')? pid=(?:&quot;|\"|')?[0-9]+[\"']?(?:&quot;|\"|')?#i", '', $username);
        $delete_quote = false;
    }
    
    unset($match);
    
    preg_match("#time=(?:&quot;|\"|')?([0-9]+)(?:&quot;|\"|')?#i", $username, $match);
    if (intval($match[1]))
    {
        $postdate = date("j F Y", intval($match[1]));
        $posttime = date("h:i A", intval($match[1]));
        $date = " ".$postdate." ".$posttime."";
        
        $username = preg_replace("#(?:&quot;|\"|')? time=(?:&quot;|\"|')?[0-9]+(?:&quot;|\"|')?#i", '', $username);
        $delete_quote = false;
    }
    
    if ($delete_quote)
    {
        $username = substr($username, 0, strlen($username)-1);
    }
    
    $span = "";
    if (!$delete_quote)
    {
        $span = $date;
    }
    
    return "<div class=\"post_quote\"><div class=\"citation\">".$linkback.htmlspecialchars($username).", on ".$span." said:</div><div class=\"content\">".$message."</div></div>";
}

function bbcode_parse_code($code)
{
    $code = str_replace("<br />", "", $code);
    
    // Clean the string before parsing.
    $code = preg_replace('#^(\t*)(\n|\r|\0|\x0B| )*#', '\\1', $code);
    $code = rtrim($code);
    $original = preg_replace('#^\t*#', '', $code);
    
    if (empty($original))
    {
        return;
    }
    
    $code = str_replace('$', '&#36;', $code);
    $code = preg_replace('#\$([0-9])#', '\\\$\\1', $code);
    $code = str_replace('\\', '&#92;', $code);
    $code = str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;', $code);
    $code = str_replace("  ", '&nbsp;&nbsp;', $code);
    $code = nl2br($code);
    return "<div class=\"b smaller\">Code:</div><div class=\"code\"><code>".$code."</code></div>";
}

function bbcode_parse_php($str, $bare_return = false)
{
    $str = str_replace("<br />", "", $str);
    $str = htmlspecialchars_decode($str);
    
    // Clean the string before parsing except tab spaces.
    $str = preg_replace('#^(\t*)(\n|\r|\0|\x0B| )*#', '\\1', $str);
    $str = rtrim($str);
    $original = preg_replace('#^\t*#', '', $str);

    if (empty($original))
    {
        return;
    }
    
    $str = str_replace('&amp;', '&', $str);
    $str = str_replace('&lt;', '<', $str);
    $str = str_replace('&gt;', '>', $str);
    
    // See if open and close tags are provided.
    $added_open_tag = false;
    
    if (!preg_match("#^\s*<\?#si", $str))
    {
        $added_open_tag = true;
        $str = "<?php \n".$str;
    }
    
    $added_end_tag = false;
    
    if (!preg_match("#\?>\s*$#si", $str))
    {
        $added_end_tag = true;
        $str = $str."\n?>";
    }
    
    $code = highlight_string($str, true);
    
    // Do the actual replacing.
    $code = preg_replace('#<code>\s*<span style="color: \#000000">\s*#i', "<code>", $code);
    $code = preg_replace("#</span>\s*</code>#", "</code>", $code);
    $code = preg_replace("#</span>(\r\n?|\n?)</code>#", "</span></code>", $code);
    $code = str_replace("\\", '&#092;', $code);
    $code = str_replace('$', '&#36;', $code);
    $code = preg_replace("#&amp;\#([0-9]+);#si", "&#$1;", $code);
    
    if ($added_open_tag)
    {
        $code = preg_replace("#<code><span style=\"color: \#([A-Z0-9]{6})\">&lt;\?php( |&nbsp;)(<br />?)#", "<code><span style=\"color: #$1\">", $code);
    }
    if ($added_end_tag)
    {
        $code = str_replace("?&gt;</span></code>", "</span></code>", $code);
        // Wait a minute. It fails highlighting? Stupid highlighter.
        $code = str_replace("?&gt;</code>", "</code>", $code);
    }
    $code = preg_replace("#<span style=\"color: \#([A-Z0-9]{6})\"></span>#", "", $code);
    $code = str_replace("<code>", "<div dir=\"ltr\"><code>", $code);
    $code = str_replace("</code>", "</code></div>", $code);
    $code = preg_replace("# *$#", "", $code);
    
    if ($bare_return)
    {
        return $code;
    }
    
    // Send back the code all nice and pretty
    return "<div class=\"b smaller\">PHP Code:\n</div><div class=\"code\"><code>".$code."</code></div>\n";
}

function highlight_xml($s)
{
    $s = preg_replace("|<[^/?](.*)\s(.*)>|isU","[1]<[2]\\1[/2] [5]\\2[/5]>[/1]",$s);
    $s = preg_replace("|</(.*)>|isU","[1]</[2]\\1[/2]>[/1]",$s);
    $s = preg_replace("|<\?(.*)\?>|isU","[3]<?\\1?>[/3]",$s);
    $s = preg_replace("|\=\"(.*)\"|isU","[6]=[/6][4]\"\\1\"[/4]",$s);
    $s = htmlspecialchars($s);
    $s = str_replace(" ", "&nbsp;", $s);
    $replace = array(1 => '0000FF', 2 => '990000', 3 => '800000', 4 => '0000FF', 5 => 'FF0000', 6 => '0000FF');
    foreach($replace as $k => $v)
    {
        $s = preg_replace("|\[".$k."\](.*)\[/".$k."\]|isU","<font color=\"".$v."\">\\1</font>", $s);
    }
    return nl2br($s);
}

//
function emoticons($text, $set = "default")
{
    $search = array(
        ":mellow:",
        ":huh:",
        "^_^", 
        ":o", 
        ";)",
        ":P", 
        ":D", 
        ":lol:",
        "B)",
        ":rolleyes:",
        "-_-",
        "<_<",
        ":)",
        ":wub:",
        ":angry:",
        ":(",
        ":unsure:",
        ":wacko:",
        ":blink:",
        ":ph34r:"
    );
    $replace = array(
        image("images/emoticons/".$set."/mellow.gif"),
        image("images/emoticons/".$set."/huh.gif"),
        image("images/emoticons/".$set."/happy.gif"),
        image("images/emoticons/".$set."/ohmy.gif"),
        image("images/emoticons/".$set."/wink.gif"),
        image("images/emoticons/".$set."/tongue.gif"),
        image("images/emoticons/".$set."/biggrin.gif"),
        image("images/emoticons/".$set."/laugh.gif"),
        image("images/emoticons/".$set."/cool.gif"),
        image("images/emoticons/".$set."/rolleyes.gif"),
        image("images/emoticons/".$set."/sleep.gif"),
        image("images/emoticons/".$set."/dry.gif"),
        image("images/emoticons/".$set."/smile.gif"),
        image("images/emoticons/".$set."/wub.gif"),
        image("images/emoticons/".$set."/angry.gif"),
        image("images/emoticons/".$set."/sad.gif"),
        image("images/emoticons/".$set."/unsure.gif"),
        image("images/emoticons/".$set."/wacko.gif"),
        image("images/emoticons/".$set."/blink.gif"),
        image("images/emoticons/".$set."/ph34r.gif")
    );
    return str_replace($search, $replace, $text);
}

// Takes a number of bytes and formats in k or MB as required
function file_size($bytes = "")
{
    if(is_numeric($bytes))
    {
        $mod = 1024;
        $units = explode(" ", "Bytes KB MB GB TB PB");
        for ($i = 0; $bytes > $mod; $i++)
        {
            $bytes /= $mod;
        }
        return round($bytes, 2)." ".$units[$i];
    }
    return "NaN";
}

// Make an SEO title for use in the URL
function make_seo_title($text)
{
	if (!$text)
	{
		return '';
	}

	// Strip all HTML tags first
	$text = strip_tags($text);

	// Preserve %data
	$text = preg_replace('#%([a-fA-F0-9][a-fA-F0-9])#', '-xx-$1-xx-', $text);
	$text = str_replace(array('%', '`'), '', $text);
	$text = preg_replace('#-xx-([a-fA-F0-9][a-fA-F0-9])-xx-#', '%$1', $text);

	// Finish off
	$text = strtolower($text);
	$text = str_replace(array('`', ' ', '+', '.', '?', '_', '#'), '-', $text);
	$text = preg_replace("#-{2,}#", '-', $text);
	$text = trim($text, '-');

	return ($text) ? $text : '-';
}

//
function add_br($msg)
{
	$msg = nl2br($msg);
	$msg = preg_replace('|[\r\n]+|si', '', $msg);
	return $msg;
}

//
function remove_br($msg)
{
	$msg = preg_replace('|<br */?>|i', "\r\n", $msg);
	return $msg;
}

//
function check($string)
{
    if (is_array($string))
    {
        foreach ($string as $key => $val)
        {
            $string[$key] = check($val);
        }
    }
    else
    {
        //$string = htmlspecialchars($string);
        $search = array('|', '\'', '$', '\\', '^', '%', '`', "\0", "\x00", "\x1A");
        $replace = array('&#124;', '&#39;', '&#36;', '&#92;', '&#94;', '&#37;', '&#96;', '', '', '');
        $string = str_replace($search, $replace, $string);
    }
    return $string;
}

function uncheck($string)
{
    if (is_array($string))
    {
        foreach ($string as $key => $val)
        {
            $string[$key] = uncheck($val);
        }
    }
    else
    {
        //$string = htmlspecialchars_decode($string);
        $search = array('&#124;', '&#39;', '&#36;', '&#92;', '&#94;', '&#37;', '&#96;');
        $replace = array('|', '\'', '$', '\\', '^', '%', '`');
        $string = str_replace($search, $replace, $string);
    }
    return $string;
}

// Function to check allowed file types
function check_file_type($file_name)
{
    $file_extension = substr($file_name, strrpos($file_name, ".") + 1);
    $file_type_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments_type WHERE atype_extension = '".$file_extension."'");
    if(mysql_numrows($file_type_query) == 1)
    {
        return true;
    }
    return false;
}

// Check email address to see if it seems valid
function check_email($email)
{
    if (substr_count($email, "@" ) > 1)
    {
        return false;
    }
    if (preg_match("#[\;\#\n\r\*\'\"<>&\%\!\(\)\{\}\[\]\?\\/\s]#", $email))
    {
        return false;
    }
    elseif (preg_match("/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,4})(\]?)$/", $email))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//
function check_url($url_val)
{
    if ($url_val == "")
    {
        return false;
    }
    else
    {
        $url_pattern = "http\:\/\/[[:alnum:]\-\.]+(\.[[:alpha:]]{2,4})+";
        $url_pattern .= "(\/[\w\-]+)*"; // folders like /val_1/45/
        $url_pattern .= "((\/[\w\-\.]+\.[[:alnum:]]{2,4})?"; // filename like index.html
        $url_pattern .= "|"; // end with filename or ?
        $url_pattern .= "\/?)"; // trailing slash or not
        if (strpos($url_val, "?"))
        {
            $url_parts = explode("?", $url_val);
            if (!preg_match("/^".$url_pattern."$/", $url_parts[0]))
            {
                return false;
            }
            if (!preg_match("/^(&?[\w\-]+=\w*)+$/", $url_parts[1]))
            {
                return false;
            }
        }
        else
        {
            if (!preg_match("/^".$url_pattern."$/", $url_val))
            {
                return false;
            }
        }
    }
    return true;
}

// Returns how long ago the specified time stamp was
// use: echo time_ago(time()-1291134087);
function time_ago($ts)
{
    //Number of seconds in a minute
	$minute = 60;

	//Number of seconds in a hour
	$hour = 3600;

	//Number of seconds in a day
	$day = 86400;

	//Number of seconds in a week
	$week = 604800;

	//Number of seconds in a year
	$year = 220752000;

	//Months with 31 days
	$months_31 = array(01, 03, 05, 07, 08, 10, 12);

	//Months with 30 days
	$months_30 = array(04, 06, 09, 11);
    
	if ($ts == time())
	{
		return '--';
	}
	if ($ts < 60)
	{
		$plural = ($ts == 1) ? '' : 's';
		return sprintf("%0d", $ts)." second$plural";
	}
    elseif ($ts < $hour)
	{
		$plural = (sprintf("%0d", ($ts/$minute)) == 1) ? '' : 's';
		return sprintf("%0d", ($ts/$minute))." minute$plural";
	}
    elseif ($ts < $day)
	{
		$plural = (sprintf("%0d", ($ts/$hour)) == 1) ? '' : 's';
		return sprintf("%0d", ($ts/$hour))." hour$plural";
	}
	else
	{
		$plural = (sprintf("%0d", ($ts/$day)) == 1) ? '' : 's';
		return sprintf("%0d", ($ts/$day))." day$plural";
	}
}


//
function is_ip_banned($ip)
{
    $ban_ip_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."banned WHERE type = 'ip' AND content = '".$ip."'");
    if (mysql_num_rows($ban_ip_query) >= 1)
    {
        return true;
    }
    return false;
}

//
function email($user_mail, $subject, $message, $from_mail = "", $from_name = "")
{
    if (function_exists("mail"))
    {
        if ($mail == "")
        {
            global $config_admin_email;
            $mail = $config_admin_email;
            $name = "Administrator";
        }
        $headers = "From: ".$name." <".$from_mail.">\r\n";
        $headers .= "X-sender: ".$from_name." <".$from_mail.">\r\n";
        $headers .= "Content-Type: text/plain; charset=utf-8\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Transfer-Encoding: 8bit\r\n";
        $headers .= "X-Mailer: PHP v.".phpversion();
        return mail($user_mail, $subject, $message, $headers);
    }
    return false;
}


//
function pagination($url, $page, $pages)
{
    if ($pages > 1)
    {
        if (strrchr($url, "?"))
        {
            $url .= "&amp;";
        }
        else
        {
            $url .=  "?";
        }
        
        //echo $pages." Pages: ";
        if ($page > 3){echo anchor($url, "&#171;", "First Page", ".button pagination")."\n";}
        if ($page > 1){echo anchor($url."page=".($page - 1), "PREV", "Previous Page", ".button pagination")."\n";}
        if ($page >= 3){echo anchor($url."page=".($page - 2), ($page - 2), ($page - 2)." Page", ".button pagination")."\n";}
        if ($page >= 2){echo anchor($url."page=".($page - 1), ($page - 1), ($page - 1)." Page", ".button pagination")."\n";}
        if ($page){echo "<span class=\"button pagination select\">".$page."</span>\n";}
        if ($page <= $pages && $page <= ($pages - 1)){echo anchor($url."page=".($page + 1), ($page + 1), ($page + 1)." Page", ".button pagination")."\n";}
        if ($page <= ($pages - 2)){echo anchor($url."page=".($page + 2), ($page + 2), ($page + 2)." Page", ".button pagination")."\n";}
        if ($page <= ($pages - 3)){echo anchor($url."page=".($page + 3), ($page + 3), ($page + 3)." Page", ".button pagination")."\n";}
        if ($page <= ($pages - 1)){echo anchor($url."page=".($page + 1), "NEXT", "Next Page", ".button pagination")."\n";}
        if ($page < ($pages - 3)){echo anchor($url."page=".$pages, "&#187;", "Last Page", ".button pagination")."\n";}
    }
}

//
function check_filename($value)
{
    $value = str_replace(" ", "_", $value);
    $value = str_replace("`", "", $value);
    $value = str_replace("]", "", $value);
    $value = str_replace("[", "", $value);
    $value = str_replace("~", "", $value);
    $value = str_replace("@", "", $value);
    $value = str_replace("#", "", $value);
    $value = str_replace("%", "", $value);
    $value = str_replace("^", "", $value);
    $value = str_replace("*", "", $value);
    $value = str_replace("|", "", $value);
    $value = str_replace("$", "", $value);
    $value = str_replace("&lt;", "", $value);
    $value = str_replace("<", "", $value);
    $value = str_replace(">", "", $value);
    $value = str_replace("&gt;", "", $value);
    $value = str_replace("\"", "", $value);
    $value = str_replace("'", "", $value);
    $value = str_replace("\\", "", $value);
    return $value;
}

//
function login_form($message = "", $type = "")
{
    echo "<form action=\"login.php\" method=\"post\">\n";
    echo "<div class=\"content\">\n";
    if ($message != "")
    {
        echo "<div class=\"message ".$type."\">\n".$message."\n</div>\n";
    }
    echo "<label for=\"username\">Username:</label><br />\n";
    echo "<input id=\"username\" type=\"text\" name=\"username\" value=\"\" maxlength=\"20\" /><br />";
    echo "<br />\n";
    echo "<label for=\"password\">Password:</label><br />\n";
    echo "<input id=\"password\" type=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br />\n";
    echo "<span class=\"desc\">".anchor("lost_password.php", "I've forgotten my password", "Retrive Password")."</span><br /><br />\n";
    echo "<input type=\"checkbox\" id=\"remember_me\" name=\"remember_me\" value=\"1\" /><label for=\"remember_me\"> Remember me</label><br />\n";
    $form_id = "wapbb".generate_form_id();
    $_SESSION["form_id"] = $form_id;
    echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
    echo "</div>\n";
    echo "<div class=\"buttons\">\n";
    $button_id = "wapbb".generate_button_id("login");
    echo "<input class=\"button ibutton\" type=\"submit\" value=\"Login\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
    echo "or ".anchor("register.php", "Register", "Create New Account")."\n";
    echo "</div>\n";
    echo "</form>\n";
}


//
function safe_encode($string)
{
	$data = base64_encode($string);
	$data = str_replace(array('+', '/', '='), array('_', '-', ''), $data);
	return $data;
}

//
function safe_decode($string)
{
	$string = str_replace(array('_', '-'), array('+', '/'), $string);
	$data = base64_decode($string);
	return $data;
}

//
function xoft_encode($string, $key)
{
	$result = "";
	for ($i = 1; $i <= strlen($string); $i++)
	{
		$char = substr($string, $i - 1, 1);
		$keychar = substr($key, ($i % strlen($key)) - 1, 1);
		$char = chr(ord($char) + ord($keychar));
		$result .= $char;
	}
	return safe_encode($result);
}

//
function xoft_decode($string, $key)
{
	$string = safe_decode($string);
	$result = "";
	for ($i = 1; $i <= strlen($string); $i++)
	{
		$char = substr($string, $i - 1, 1);
		$keychar = substr($key, ($i % strlen($key)) - 1, 1);
		$char = chr(ord($char) - ord($keychar));
		$result .= $char;
	}
	return $result;
}

//
function generate_form_id()
{
    return md5(uniqid(mt_rand(), true));
}

function generate_button_id($prefix = "")
{
    return md5(SITE_URL.$prefix.get_ip().date("HDY"));
}

//
function generate_captcha($length = 4)
{
	$randstr = "";
	for ($i = 0; $i < $length; $i++)
	{
		$randnum = mt_rand(0, 61);
		if ($randnum < 10)
		{
			$randstr .= chr($randnum + 48);
		}
		elseif ($randnum < 36)
        {
            $randstr .= chr($randnum + 55);
        }
        else
        {
            $randstr .= chr($randnum + 61);
        }
	}
	return $randstr;
}

//
function error_handler($errno, $errstr, $errfile, $errline)
{
    /* Did we turn off errors with @? */
    if (!error_reporting())
    {
        return;
    }
    
    /* Are we truly debugging? */
    if (IPS_ERROR_CAPTURE === FALSE)
    {
        return;
    }
    
    $errfile = str_replace( @getcwd(), "", $errfile );
    $log	 = false;
    $message = "> [$errno] $errstr\n> > Line: $errline\n> > File: $errfile";
    
    /* What do we have? */
    switch ($errno)
    {
        case E_ERROR:
            $log = true;
            echo "<b>IPB ERROR</b> [$errno] $errstr (Line: $errline of $errfile)<br />\n";
            exit(1);
            break;
        case E_WARNING:
            $log = true;
            echo "<b>IPB WARNING</b> [$errno] $errstr (Line: $errline of $errfile)<br />\n";
            break;
        case E_NOTICE:
            $log = true;
            break;
        default:
            return FALSE; //Do nothing
            break;
    }
    
    /* Logging? */
    if ($log)
    {
        if ( IPS_ERROR_CAPTURE === TRUE )
        {
            self::addLogMessage( $message, "phpNotices", false, true );
        }
        else
        {
            foreach(explode(',', IPS_ERROR_CAPTURE ) as $class)
            {
                if (preg_match("#/".preg_quote($class, '#')."\.#", $errfile))
                {
                    self::addLogMessage( $message, "phpNotices", false, true );
                    break;
                }
            }
        }
    }
}
?>