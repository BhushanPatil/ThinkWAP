<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";

$time = time() + ($config_time_clocks * 3600);
$ip = get_ip();
$ua = get_ua();
$php_self = $_SERVER["PHP_SELF"];

if (is_logged())
{
	set_time_limit(0);
	if (isset($_GET["aid"]) || !empty($_GET["aid"]))
	{
		$attach_id = $_GET["aid"];
		$attach_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments WHERE attach_id = '".$attach_id."'");
		if ($attach = mysql_fetch_array($attach_query))
		{
			$file_name = basename($attach["attach_file"]);
			$file_loaction = $attach["attach_location"];
			if (file_exists($file_loaction) && is_file($file_loaction))
			{
				$file_size = filesize($file_loaction);
				$file_ext = strtolower(substr(strrchr($file_name, "."), 1));
				$attach_type_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."attachments_type WHERE atype_extension = '".$file_ext."'");
				if ($attach_type = mysql_fetch_array($attach_type_query))
				{
					$mime_type = $attach_type["atype_mimetype"];
					if ($mime_type == "")
					{
						$mime_type = "application/force-download";
					}
				}
				else
				{
					$mime_type = "application/force-download";
				}
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Type: ".$mime_type);
				header("Content-Disposition: attachment; filename=\"".$file_name."\"");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".$file_size);
				$file = fopen($file_loaction, "rb");
				if ($file)
				{
					while (!feof($file))
					{
						print (fread($file, 1024 * 8));
						flush();
						if (connection_status() != 0)
						{
							fclose($file);
							mysql_query("UPDATE ".SQL_TABLE_PERFIX."attachments SET attach_hits = attach_hits + 1 WHERE attach_id = '".$attach_id."'");
							die();
						}
					}
					fclose($file);
					mysql_query("UPDATE ".SQL_TABLE_PERFIX."attachments SET attach_hits = attach_hits + 1 WHERE attach_id = '".$attach_id."'");
				}
			}
			else
			{
				header("Location: ".SITE_URL);
				exit;
			}
		}
		else
		{
			header("Location: ".SITE_URL);
			exit;
		}
	}
	else
	{
		header("Location: ".SITE_URL);
		exit;
	}
}
else
{
	include_once "themes/".$config_theme."/index.php";
	login_form("You must log in to view this attachment.", "notice");
	include_once "themes/".$config_theme."/foot.php";
}
?>