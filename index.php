<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

$forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '0' ORDER BY forum_position ASC");
while ($forum = mysql_fetch_array($forum_query))
{
    echo "<div class=\"list\">\n";
    echo "<div class=\"title\">".anchor("forum.php?fid=".$forum["forum_id"], htmlspecialchars($forum["forum_name"]), htmlspecialchars("View ".$forum["forum_name"]))."</div>\n";
    $sub_forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$forum["forum_id"]."' ORDER BY forum_position ASC");
    while ($sub_forum = mysql_fetch_array($sub_forum_query))
    {
        //echo "<div class=\"row\" onclick=\"location.href='forum.php?fid=".$sub_forum["forum_id"]."';\">\n";
        echo "<div class=\"row\">\n";
        echo "<div>\n";
        $img = "forum";
        if (is_logged())
        {
            if ($sub_forum["num_topics"] > 0)
            {
                $count_topic_user_map_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."topic_user_map WHERE user_id = '".$user["user_id"]."' AND topic_forum_id = '".$sub_forum["forum_id"]."'");
                $count_topic_user_map = mysql_fetch_array($count_topic_user_map_query);
                if ($sub_forum["num_topics"] > $count_topic_user_map[0])
                {
                    $img .= "_unread";
                    $img_alt = "Unread";
                }
                else
                {
                    $topic_user_map_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topic_user_map WHERE user_id = '".$user["user_id"]."' AND topic_forum_id = '".$sub_forum["forum_id"]."'");
                    if (mysql_numrows($topic_user_map_query) == 0)
                    {
                        $img .= "_unread";
                        $img_alt = "Unread";
                    }
                    else
                    {
                        $topic_user_map = mysql_fetch_array($topic_user_map_query);
                        if ($topic_user_map["has_read"] == 0)
                        {
                            $img .= "_unread";
                            $img_alt = "Unread";
                        }
                        else
                        {
                            $img .= "_read";
                            $img_alt = "Read";
                        }
                    }
                }
            }
            else
            {
                $img .= "_read";
                $img_alt = "Read";
            }
        }
        else
        {
            $img .= "_unread";
            $img_alt = "Unread";
            
        }
        
        echo anchor("forum.php?fid=".$sub_forum["forum_id"], image("images/".$img.".png", $img_alt),  "View ".$sub_forum["forum_name"])."\n";
        
        //echo anchor("forum.php?fid=".$sub_forum["forum_id"], image("images/f_unread.png", "Unread Froum"), "View ".$forum["forum_name"])."\n";
        echo anchor("forum.php?fid=".$sub_forum["forum_id"], htmlspecialchars($sub_forum["forum_name"]), "Go to ".$sub_forum["forum_name"]." forum", ".b")."\n";
        echo "</div>\n";
        //echo "<div class=\"row-arrow\" style=\"margin-top:0px !important\"></div>";
        echo "<div class=\"desc\" style=\"margin-left: 30px;\">".$sub_forum["num_topics"]." Topics &middot; ".$sub_forum["num_posts"]." Posts</div>\n";
        
        /*
        $sub_sub_forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$sub_forum["forum_id"]."' ORDER BY forum_position ASC");
        
        if (mysql_num_rows($sub_sub_forum_query) > 0)
        {
            echo "<div style=\"margin-left: 30px;\">\n";
            while ($sub_sub_forum = mysql_fetch_array($sub_sub_forum_query))
            {
                 echo "&#9679; ".anchor("forum.php?fid=".$sub_sub_forum["forum_id"], htmlspecialchars($sub_sub_forum["forum_name"]), "Go to ".$sub_sub_forum["forum_name"])."\n";
            }
            echo "</div>\n";
        }
        //*/
        echo "</div>\n";
    }
    echo "</div>\n";
}

echo "<div class=\"list\">\n";
echo "<div class=\"title center\">".anchor("top_posters.php", "Top Posters", "View Top Posters!")." - ".anchor("leader.php", "Forum Leaders", "View Forum Leaders!")."</div>\n";
echo "<div class=\"info\">\n";
echo anchor("online.php", "Users online", "View Online Users!", ".b").": 0 members and 0 guests.<br />\n";
echo "Total posts: <b>0</b><br />\n";
$count_members = count_members();
echo "Total registered member: <b>".$count_members."</b><br />\n";
if ($count_members > 0)
{
    $last_member = last_member();
    echo "Newest registered member: ".anchor("user.php?uid=".$last_member["user_id"], $last_member["username"], $last_member["username"]."'s Profile", ".b")."<br />\n";
}
echo "Maximum online at once record: <b>1</b>\n";

echo "</div>\n";
echo "</div>\n";
include_once "themes/".$config_theme."/foot.php";
?>