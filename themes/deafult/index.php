<?php
if (!defined("WAPBB"))
{
    exit;
}
if (strtok($ua, "/") != "Mozilla")
{
    header("Content-type: application/vnd.wap.xhtml+xml; charset=utf-8");
}
else
{
    header("Content-type: text/html; charset=utf-8");
}
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
echo "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.2//EN\" \"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd\">\n";
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n";
echo "<head>\n";
echo "<meta http-equiv=\"Content-Type\" content=\"application/vnd.wap.xhtml+xml; charset=UTF-8\" />\n";
echo "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />\n";
echo "<link rel=\"shortcut icon\" href=\"".$config_favicon."\" />\n";
echo "<link rel=\"stylesheet\" href=\"".SITE_URL."/themes/deafult/style.css\" type=\"text/css\"  media=\"handheld, screen, projection, print\" />\n";
if (stristr($php_self, "index.php"))
{
    echo "<title>".htmlspecialchars(SITE_TITLE." - ".$config_slogan)."</title>\n";
}
elseif ($my_title != "")
{
    echo "<title>".htmlspecialchars($my_title)."</title>\n";
    //echo "<title>".htmlspecialchars($my_title." - ".SITE_TITLE)."</title>\n";
}
else
{
    echo "<title>".htmlspecialchars(SITE_TITLE)."</title>\n";
}
echo "</head>\n";
echo "<body>\n";
echo "<div class=\"page\">\n";

echo "<div class=\"header\" id=\"top\">\n";

if (is_logged())
{
    echo "<div class=\"panel\">\n";
    echo "Login as ".anchor("user.php?uid=".$user["user_id"], htmlspecialchars($user["username"]), "Your Profile")." &ndash; ".anchor("logout.php", "Logout", false)."";
    echo "</div>\n";
}

echo "<div class=\"logo\">\n";

echo "<span class=\"i b\">\n";
echo anchor(SITE_URL, SITE_TITLE, SITE_TITLE)."\n";
echo "</span>\n";

echo "</div>\n";




/*
if (is_logged())
{
    echo "<div class=\"center i\">\n";
    echo "Login as ".anchor("user.php?uid=".$user["user_id"], htmlspecialchars($user["username"]), "Your Profile");
    echo " (".anchor("logout.php", "Logout", false).")";
    echo "</div>\n";
}
//*/

echo "<div class=\"menu\">\n";

if (stristr($php_self, "index.php") || stristr($php_self, "forum.php") || stristr($php_self, "topic.php") || stristr($php_self, "reply.php") || stristr($php_self, "attachments.php"))
{
    echo anchor(SITE_URL, "Forums", "Forums", ".current");
}
else
{
    echo anchor(SITE_URL, "Forums", "Go to Forums");
}
if (stristr($php_self, "search.php"))
{
    echo anchor("search.php", "Search", "Search", ".current");
}
else
{
    echo anchor("search.php", "Search", "Search ".SITE_TITLE);
}


if (is_logged())
{
    if (stristr($php_self, "pm.php"))
    {
        echo anchor("pm.php", "PMs", "Inbox", ".current");
    }
    else
    {
        echo anchor("pm.php", "PMs", "Read My Messages");
    }
    if (stristr($php_self, "usercp.php"))
    {
        echo anchor("usercp.php", "UCP", "Edit My Settings", ".current");
    }
    else
    {
        echo anchor("usercp.php", "UCP", "Edit My Settings");
    }
    
    /*
    if (stristr($php_self, "logout.php"))
    {
        echo anchor("logout.php", "Logout", "Logout", ".current");
    }
    else
    {
        echo anchor("logout.php", "Logout", "Logout");
    }
    //*/
}

else
{
    if (stristr($php_self, "login.php") || stristr($php_self, "lost_password.php"))
    {
        echo anchor("login.php", "Login", "Login", ".current");
    }
    else
    {
        echo anchor("login.php", "Login", "Login to your account");
    }
    if (stristr($php_self, "Register.php"))
    {
        echo anchor("register.php", "Register", "Register", ".current");
    }
    else
    {
        echo anchor("register.php", "Register", "Create new account");
    }
}


echo "\n</div>\n";

echo "</div>\n";

if ($config_forum_navbar == 1 && $navbar != "")
{
    echo "<div class=\"navbar\">".$navbar."</div>\n";
}


///*
echo "<div class=\"content\">\n";
echo "<div class=\"message unspecific\">\n";
foreach ($_GET as $name => $value)
{
    echo "<b>GET</b>: ".$name.": ".$value."<br />\n";
}
foreach ($_POST as $name => $value)
{
    echo "<b>POST</b>: ".$name.": ".$value."<br />\n";
}
foreach ($_SESSION as $name => $value)
{
    echo "<b>SESSION</b>: ".$name.": ".$value."<br />\n";
}
foreach ($_COOKIE as $name => $value)
{
    echo "<b>COOKIE</b>: ".$name.": ".$value."<br />\n";
}
echo "\n</div>\n";
echo "\n</div>\n";
//*/
?>