<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

if (isset($_GET["page"]))
{
    $page = (int)$_GET["page"];
}
else
{
    $page = 1;
}
if ($forum["parent_id"] == 0)
{
    echo "<div class=\"list\">\n";
    echo "<div class=\"title\">".anchor("forum.php?fid=".$forum_id, htmlspecialchars($forum["forum_name"]), "View ".$forum["forum_name"])."</div>\n";
    $forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$forum_id."' ORDER BY forum_position ASC");
    while ($forum = mysql_fetch_array($forum_query))
    {
        echo "<div class=\"row\">\n";
        echo "<div>\n";
        echo anchor("forum.php?fid=".$forum["forum_id"], image("images/f_unread.png", "Unread"), "Unread Forum")."\n";
        echo anchor("forum.php?fid=".$forum["forum_id"], htmlspecialchars($forum["forum_name"]), "Go to ".$forum["forum_name"], ".b")."\n";
        echo "</div>\n";
        echo "<div class=\"desc\" style=\"margin-left: 30px;\">".$forum["num_topics"]." Topics &middot; ".$forum["num_posts"]." Posts</div>\n";
        echo "</div>\n";
    }
    echo "</div>\n";
}
else
{
    /*
    $sub_forum_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."forums WHERE parent_id = '".$forum_id."' ORDER BY forum_position ASC");
    if (mysql_num_rows($sub_forum_query) > 0)
    {
        echo "<div class=\"list\">\n";
        echo "<div class=\"title\">Subforums:</div>\n";
        while ($sub_forum = mysql_fetch_array($sub_forum_query))
        {
            echo "<div class=\"row\">\n";
            echo "<div>\n";
            echo anchor("forum.php?fid=".$sub_forum["forum_id"], image("images/f_unread.png", "Unread"), "Unread Forum")."\n";
            echo anchor("forum.php?fid=".$sub_forum["forum_id"], htmlspecialchars($sub_forum["forum_name"]), "Go to ".$sub_forum["forum_name"], ".b")."\n";
            echo "<div class=\"desc\" style=\"margin-left: 30px;\">".$sub_forum["num_topics"]." Topics &middot; ".$sub_forum["num_posts"]." Posts</div>\n";
            echo "</div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
    }
    //*/
    
    echo "<div class=\"content\">".image("images/page_add.png")." ".anchor("new_topic.php?fid=".$forum_id, "Start New Topic", "Start New Topic", ".b")."</div>\n";

    echo "<div class=\"list\">\n";
    echo "<div class=\"title\">".anchor("forum.php?fid=".$forum_id, htmlspecialchars($forum["forum_name"]), "Go to ".$forum["forum_name"])."</div>\n";
    $topic_count_query = mysql_query("SELECT COUNT(*) FROM ".SQL_TABLE_PERFIX."topics WHERE topic_forum_id = '".$forum_id."'");
    $topic_count = mysql_fetch_array($topic_count_query);
    $topic_count = $topic_count[0];
    $pages = ceil($topic_count / $config_topics_per_page);
    $count_start = ($page - 1) * $config_topics_per_page;
    $topic_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topics WHERE topic_forum_id = '".$forum_id."' ORDER BY pinned DESC, last_post_time DESC LIMIT ".$count_start.", ".$config_topics_per_page."");
    if ($topic_count > 0)
    {
        while ($topic = mysql_fetch_array($topic_query))
        {
            if ($topic["pinned"] == 1)
            {
                echo "<div class=\"row odd\">\n";
            }
            else
            {
                echo "<div class=\"row\">\n";
            }
            echo "<div>\n";
            $img = "topic";
            if ($topic["closed"] == 0)
            {
                if ($topic["num_posts"] > 20)
                {
                    $img .= "_hot";
                }
                
                if (is_logged())
                {
                    $is_user_reply_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."posts WHERE poster_id = '".$user["user_id"]."' AND post_topic_id = '".$topic["topic_id"]."'");
                    if (mysql_numrows($is_user_reply_query) >= 1)
                    {
                        $img .= "_dot";
                    }
                    $topic_user_map_query = mysql_query("SELECT * FROM ".SQL_TABLE_PERFIX."topic_user_map WHERE user_id = '".$user["user_id"]."' AND topic_id = '".$topic["topic_id"]."'");
                    if (mysql_numrows($topic_user_map_query) == 0)
                    {
                        $img .= "_unread";
                        $img_alt = "Unread";
                    }
                    else
                    {
                        $topic_user_map = mysql_fetch_array($topic_user_map_query);
                        if ($topic_user_map["has_read"] == 0)
                        {
                            $img .= "_unread";
                            $img_alt = "Unread";
                        }
                        else
                        {
                            $img .= "_read";
                            $img_alt = "Read";
                        }
                    }
                }
                else
                {
                    $img .= "_unread";
                    $img_alt = "Unread";
                }
            }
            else
            {
                $img .= "_closed";
                $img_alt = "Closed";
            }
            echo anchor("topic.php?tid=".$topic["topic_id"], image("images/".$img.".png", $img_alt), $img_alt." Topic")."\n";
            echo anchor("topic.php?tid=".$topic["topic_id"], htmlspecialchars($topic["title"]), "View ".$topic["title"], ".b")."\n";
            echo "</div>\n";
            if ($topic["num_posts"] > 0)
            {
                echo "<div class=\"desc\" style=\"margin-left: 30px;\">".$topic["num_posts"]." Replies: Last by ".$topic["last_poster_name"].", ".date("M j Y h:i A", $topic["last_post_time"])."</div>\n";
            }
            else
            {
                //echo "<div class=\"desc\" style=\"margin-left: 30px;\">0 Replies: ".anchor("reply.php?tid=".$topic["topic_id"], "Add Reply", "Add new Reply", ".smaller")."</div>\n";
                echo "<div class=\"desc\" style=\"margin-left: 30px;\">0 Replies</div>\n";
                //echo "<div class=\"desc\" style=\"margin-left: 30px;\">0 Replies: Last by -</div>\n";
            }
            echo "</div>\n";
        }
    }
    else
    {
        echo "<div class=\"row\">\n";
        echo "<div class=\"desc\">No topics were found. This is either because there are no topics in this forum.</div>\n";
        echo "</div>\n";
    }
    echo "</div>\n";
    if ($topic_count > $config_topics_per_page)
    {
        echo "<div class=\"pagination\">\n";
        pagination("forum.php?fid=".$forum_id, $page, $pages);
        echo "</div>\n";
    }
    echo "<div class=\"content\">".image("images/page_add.png")." ".anchor("new_topic.php?fid=".$forum_id, "Start New Topic", "Start New Topic", ".b")."</div>\n";
}
include_once "themes/".$config_theme."/foot.php";
?>