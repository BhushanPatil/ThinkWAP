<?php

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

echo "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.2//EN\" \"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd\">\n";
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n";
echo "<head>\n";
echo "<meta http-equiv=\"Content-Type\" content=\"application/vnd.wap.xhtml+xml; charset=UTF-8\" />\n";
echo "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />\n";
echo "<link rel=\"shortcut icon\" href=\"favicon.ico\" />\n";
echo "<link rel=\"stylesheet\" href=\"css/default.css\" type=\"text/css\" />\n";
echo "<title>".$my_title."</title>\n";
echo "</head>\n";
echo "<body>\n";
echo "<div class=\"page\">\n";
echo "<div class=\"header\">\n";
echo "<table width=\"100%\">\n";
echo "<tr>\n";
echo "<td align=\"left\">".image("images/logo.png", "wapBB")."</td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</div>\n";

echo "<div class=\"tabs\">\n";
echo "<span class=\"tab select\">Installation</span>\n";
echo "</div>\n";

if ($_GET["step"] == "set")
{
    echo "<form action=\"install.php?step=db\" method=\"post\">";
    echo "<label for=\"host\">SQL Host:</label><br /><input type=\"text\" name=\"host\" id=\"host\" value=\"localhost\" /><br /><br />\n";
    echo "<label for=\"name\">Database Name:</label><br /><input type=\"text\" name=\"name\" id=\"name\" value=\"".$_POST["name"]."\" /><br /><br />\n";
    echo "<label for=\"user\">SQL Username:</label><br /><input type=\"text\" name=\"user\" id=\"username\" value=\"".$_POST["user"]."\" /><br /><br />\n";
    echo "<label for=\"pass\">SQL Password:</label><br /><input type=\"text\" name=\"pass\" id=\"password\" value=\"".$_POST["pass"]."\" /><br /><br />\n";
    echo "<label for=\"prefix\">SQL Table Prefix:</label><br /><input type=\"text\" name=\"prefix\" id=\"prefix\" value=\"wapbb_\" /><br /><br />\n";
    echo "<label for=\"url\">Site URL:</label><br /><input type=\"text\" name=\"url\" id=\"url\" value=\"wapbb_\" /><br /><br />\n";
    echo "<div class=\"buttons center\"><input class=\"ibutton\" type=\"submit\" name=\"check\" value=\"Next &gt;\" /></div>\n";
    echo "</form>\n";
}
elseif ($_GET["step"] == "db")
{
	echo "<div class=\"navbar\">Database Details</div>\n";
	echo "<div class=\"content\">\n";
	$connection = 0;
	if ($_POST["check"])
	{
		if (mysql_connect($_POST["host"], $_POST["user"], $_POST["pass"]))
		{
			if (mysql_select_db($_POST["name"]))
			{
				$connection = 1;
				$config = array("SQL_HOST" => $_POST["host"],
                                "SQL_DATABASE" => $_POST["name"],
                                "SQL_USERNAME" => $_POST["user"],
                                "SQL_PASSWORD" => $_POST["pass"],
                                "SQL_TABLE_PERFIX" => $_POST["prefix"]
                                );
				$config_file = "<?php\n\n";
				foreach ($config as $k => $v)
				{
					$config_file .= "define(\"".$k."\",\"".$v."\");\n";
				}
				$config_file .= "\n?>";
				$fh = @fopen("includes/config_sql.php", "w");
				@fwrite($fh, $config_file, strlen($config_file));
				@fclose($fh);
			}
			else
			{
				echo "<div class=\"message error\">Could not select MySQL dtabase</div>";
			}
		}
		else
		{
			echo "<div class=\"message error\">Could not connect to MySQL server</div>";
		}
	}
	if ($connection == 1)
	{
        $settings_table = mysql_query("CREATE TABLE ".$_POST["prefix"]."settings (
  ID BIGINT(20) NOT NULL AUTO_INCREMENT,
  Title VARCHAR(255) NOT NULL DEFAULT '',
  Value VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        if ($settings_table)
        {
            echo "settings table installed successfully<br />";
        }
        echo "<form action=\"install.php?step=set\" method=\"post\">";
        echo "<div class=\"buttons center\"><input class=\"ibutton\" type=\"submit\" value=\"Next &gt;\" /></div>\n";
        echo "</form>\n";
	}
	else
	{
		echo "<form action=\"install.php?step=db\" method=\"post\">";
		echo "<label for=\"host\">SQL Host:</label><br /><input type=\"text\" name=\"host\" id=\"host\" value=\"localhost\" /><br /><br />\n";
		echo "<label for=\"name\">Database Name:</label><br /><input type=\"text\" name=\"name\" id=\"name\" value=\"".$_POST["name"]."\" /><br /><br />\n";
		echo "<label for=\"user\">SQL Username:</label><br /><input type=\"text\" name=\"user\" id=\"username\" value=\"".$_POST["user"]."\" /><br /><br />\n";
		echo "<label for=\"pass\">SQL Password:</label><br /><input type=\"text\" name=\"pass\" id=\"password\" value=\"".$_POST["pass"]."\" /><br /><br />\n";
		echo "<label for=\"prefix\">SQL Table Prefix:</label><br /><input type=\"text\" name=\"prefix\" id=\"prefix\" value=\"wapbb_\" /><br /><br />\n";
        echo "<label for=\"url\">Site URL:</label><br /><input type=\"text\" name=\"url\" id=\"url\" value=\"wapbb_\" /><br /><br />\n";
		echo "<div class=\"buttons center\"><input class=\"ibutton\" type=\"submit\" name=\"check\" value=\"Next &gt;\" /></div>\n";
		echo "</form>\n";
	}
	echo "</div>\n";
}
else
{
	echo "<div class=\"navbar\">End User License Agreement</div>\n";
	echo "<form action=\"install.php?step=db\" method=\"post\">";
    echo "<div class=\"content\">\n";
    echo "Please read and agree to the End User License Agreement before continuing.<br />\n";
	echo "<div class=\"eula\">";
	echo @implode("<br />", file("License.txt"));
	echo "</div>\n";
    echo "</div>\n";
	echo "<div class=\"buttons center\"><input class=\"ibutton\" type=\"submit\" value=\"I agree to the license agreement\" /></div>\n";
	echo "</form>\n";
}

echo "</div>\n";
echo "<div class=\"copy center\" id=\"down\">\n";
echo "Powered by <a href=\"htt://thinkwap.tk\">wapBB</a>\n";
echo "</div>\n";
echo "</body>\n";
echo "</html>\n";
exit;

?>