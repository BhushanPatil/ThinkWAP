<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";
include_once "themes/".$config_theme."/index.php";

if (is_logged())
{
    if (isset($_GET["pid"]))
    {
        $error = array();
        if (isset($_SESSION["form_id"]))
        {
            $old_form_id = $_SESSION["form_id"];
        }
        else
        {
            $old_form_id = "";
        }
        $form_id = "wapbb".generate_form_id();
        $_SESSION["form_id"] = $form_id;
        $button_id = "wapbb".generate_button_id("editpost");
        if (isset($_POST[$button_id]))
        {
            if ($old_form_id != $_POST["form_id"])
            {
                $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
            }
            
            $post_content = $_POST["post"];
            if ($post_content == "")
            {
                $error["post"] = "You must enter a post";
            }
            elseif (strlen($post_content) > 2000)
            {
                $error["post"] = "Post must be at most 2000 characters in length";
            }
            
            if (isset($_POST["use_emo"]))
			{
				$use_emo = 1;
			}
			else
			{
				$use_emo = 0;
			}
            
            if (isset($_POST["use_sig"]))
			{
				$use_sig = 1;
			}
			else
			{
				$use_sig = 1;
			}
            
            if (empty($error))
            {
                mysql_query("UPDATE ".SQL_TABLE_PERFIX."posts SET post_content = '".$post_content."', edit_time = '".$time."' WHERE post_id = '".$post_id."'");
                echo "<div class=\"content\">\n";
                echo "<div class=\"message\">\n";
                echo "<b>Reply added successfully!</b><br />\n";
                $total_post = $topic["num_posts"] + 1;
                if ($config_posts_per_page < $total_post)
                {
                    $last_page = ceil($total_post / $config_posts_per_page);
                }
                else
                {
                    $last_page = 1;
                }
                echo anchor("topic.php?tid=".$post["post_topic_id"]."&amp;page=".$last_page, "Click here to view", "View Topic")."\n";
                echo "</div>\n";
                echo "</div>\n";
                include_once "themes/".$config_theme."/foot.php";
            }
        }
        elseif (isset($_POST["previewpost"]))
        {
            $post_content = $_POST["post"];
            $post_content = reverse_mysql_real_escape_string($post_content);
            echo "<div class=\"title\">Post Preview</div>\n";
            echo "<div class=\"posts\">\n";
            if (empty($post))
            {
                echo "<div class=\"message error\">You must enter a post</div>\n";
            }
            else
            {
                echo "<div class=\"post\">\n";
                echo "<div class=\"post_author\">\n";
                echo image("images/default_thumb.png", null, null, null, ".photo off")."\n";
                echo anchor("user.php?uid=".$user["user_id"], htmlspecialchars($user["username"]), "View ".$user["username"]."'s Profile", ".b")."<br />\n";
                echo "<span class=\"desc smaller\">".date("j F Y - h:i A", $time)."</span>";
                echo "</div>\n";
                echo "<div class=\"post_content\">\n";
                if (isset($_POST["use_emo"]))
                {
                    echo bbcode($post_content)."<br />\n";
                }
                else
                {
                    echo bbcode($post_content, false)."<br />\n";
                }
                echo "</div>\n";
                echo "</div>\n";
            }
            echo "</div>\n";
        }
        else
        {
            $post_content = $post["post_content"];
        }
        echo "<div class=\"title\">Editing a post in ".htmlspecialchars($topic["title"])."</div>\n";
        echo "<form method=\"post\" action=\"editpost.php?pid=".$post_id."\">\n";
        echo "<div class=\"content\">\n";
        if (!empty($error["form"]))
        {
            echo "<div class=\"message error\">\n";
            echo $error["form"]."<br />\n";
            echo "</div>\n";
        }
        elseif (!empty($error["post"]))
        {
            echo "<div class=\"message notice\">\n";
            echo $error["post"]."<br />\n";
            echo "</div>\n";
        }
        echo "<label for=\"post\">Post:</label><br />\n";
        echo "<textarea id=\"post\" name=\"post\" rows=\"10\" cols=\"40\">".htmlspecialchars($post_content)."</textarea><br /><br />\n";
        echo "<input type=\"checkbox\" checked=\"checked\" id=\"use_emo\" name=\"use_emo\" value=\"1\" /> <label for=\"use_emo\">Enable emoticons?</label><br />\n";
       // echo "<input type=\"checkbox\" checked=\"checked\" id=\"use_sig\" name=\"use_sig\" value=\"1\" /> <label for=\"use_sig\">Enable signature?</label><br />\n";
        echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
        echo "</div>\n";
        echo "<div class=\"buttons\">\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Update Post\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
        echo "<input class=\"button ibutton\" type=\"submit\" value=\"Preview Post\" name=\"previewpost\" id=\"".$button_id."\" />\n";
        //echo " or ".anchor("topic.php?tid=".$topic_id, "Cancel", "Cancel")."\n";
        echo "</div>\n";
        echo "</form>\n";
    }
}
else
{
    login_form("You must log in to edit the post", "notice");
}

include_once "themes/".$config_theme."/foot.php";
?>