<?php

$code = $_GET["code"]; //the error code
$code .= ""; //avoid integer indexing of the array
 
$errors = array("300" => "Multiple Choices",
                "301" => "Moved Permanently",
                "302" => "Moved Temporarily",
                "303" => "See Other",
                "304" => "Not Modified",
                "305" => "Use Proxy",
                "400" => "Bad Request",
                "401" => "Authorization Required",
                "402" => "Payment Required",
                "403" => "Forbidden",
                "404" => "Not Found",
                "405" => "Method Not Allowed",
                "406" => "Not Acceptable",
                "407" => "Proxy Authentication Required",
                "408" => "Request Timed Out",
                "409" => "Conflicting Request",
                "410" => "Gone",
                "411" => "Content Length Required",
                "412" => "Precondition Failed",
                "413" => "Request Entity Too Long",
                "414" => "Request URI Too Long",
                "415" => "Unsupported Media Type",
                "500" => "Internal Server Error",
                "501" => "Not Implemented",
                "502" => "Bad Gateway",
                "503" => "Service Unavailable",
                "504" => "Gateway Timeout",
                "505" => "HTTP Version Not Supported",
                "ip_banned" => "Your IP is banned",
                "sql_error" => "Could not connect to database server"
                );
echo $errors[$code];

?>