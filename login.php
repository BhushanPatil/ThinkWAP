<?php
require_once "includes/start.php";
require_once "includes/functions.php";
require_once "includes/header.php";

if (is_logged())
{
	header("Location: ".SITE_URL);
	exit;
}
if (isset($_SESSION["form_id"]))
{
    $old_form_id = $_SESSION["form_id"];
}
else
{
    $old_form_id = "";
}
$form_id = "wapbb".generate_form_id();
$_SESSION["form_id"] = $form_id;
$button_id = "wapbb".generate_button_id("login");
if (isset($_POST[$button_id]))
{
    $error = array();
    if ($old_form_id != $_POST["form_id"])
    {
        $error["form"] = "<b>Form ID Value Mismatch:</b><br />\nThe submitted ID does not match registered ID of this form.";
    }
    
    $username = strip_tags($_POST["username"]);
    $password = strip_tags($_POST["password"]);
    
    if ($username == "")
    {
        $error["username"] = "Please enter your username";
    }
    elseif (!preg_match("/^[A-Za-z0-9_-]{3,20}+$/", $username))
    {
        $error["username"] = "Invalid username";
    }
    if ($password == "")
    {
        $error["password"] = "Please enter your password";
    }
    if (isset($_POST["remember_me"]))
    {
        $remember_me = 1;
    }
    else
    {
        $remember_me = 0;
    }
    if (empty($error))
    {
        if (login($username, $password) == true)
        {
            $user = get_user($username);
            $username = $user["username"];
            $_SESSION["username"] = $username;
            mysql_query("UPDATE ".SQL_TABLE_PERFIX."users SET last_visit = '".$time."' WHERE username = '".$username."'");
            if ($remember_me == 1)
            {
                $log_user = xoft_encode($username, $config_keypass);
                $log_pass = xoft_encode($password, $config_keypass);
                $expire = $time + (60 * 60 * 24 * 14);
                $domain = get_host(SITE_URL);
                $secure = isset($_SERVER["HTTPS"]);
                setcookie("log_user", $log_user, $expire, "/", $domain, $secure, true);
                setcookie("log_pass", $log_pass, $expire, "/", $domain, $secure, true);
            }
            header("Location: ".SITE_URL);
            exit;
        }
		else
		{
			$error["login"] = "Incorrect username or password";
		}
	}
}
else
{
	$username = "";
	$password = "";
}

include_once "themes/".$config_theme."/index.php";
echo "<form action=\"login.php\" method=\"post\">\n";
echo "<div class=\"content\">\n";
if (!empty($error["form"]))
{
	echo "<div class=\"message error\">".$error["form"]."</div>\n";
}
elseif (!empty($error["login"]))
{
	echo "<div class=\"message error\">".$error["login"]."</div>\n";
}
echo "<label for=\"username\">Username:</label><br />\n";
echo "<input id=\"username\" type=\"text\" name=\"username\" value=\"".$username."\" maxlength=\"20\" /><br />";
if (!empty($error["username"]))
{
	echo "<span class=\"desc error\">".$error["username"]."</span><br />";
}
echo "<br />\n";
echo "<label for=\"password\">Password:</label><br />\n";
echo "<input id=\"password\" type=\"password\" name=\"password\" value=\"\" maxlength=\"32\" /><br />\n";
if (!empty($error["password"]))
{
	echo "<span class=\"desc error\">".$error["password"]."</span><br />\n";
}
echo "<span class=\"desc\">".anchor("lost_password.php", "I've forgotten my password", "Retrive Password")."</span><br /><br />\n";
echo "<input type=\"checkbox\" id=\"remember_me\" name=\"remember_me\" value=\"1\" /> <label for=\"remember_me\">Remember me</label><br />\n";
echo "<input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"".$form_id."\" />\n";
echo "</div>\n";
echo "<div class=\"buttons\">\n";
echo "<input class=\"button ibutton\" type=\"submit\" value=\"Login\" name=\"".$button_id."\" id=\"".$button_id."\" />\n";
echo "or ".anchor("register.php", "Register", "Create new account")."\n";
echo "</div>\n";
echo "</form>\n";
include_once "themes/".$config_theme."/foot.php";
?>